// payments
export const LS_ONBOARDING_KEY = "crd_onbrd";
export const CURRENCY = "MT";
export const DASHBOARD_NUM_PAYMENTS = 5;
export const ONBOARDING_INIT_AMOUNT = "35";

// favorites
export const FAVOURITE_LIMIT = 5;

// i18n
export const DEFAULT_LANGUAGE = "pt";

//business rules
export const SMARTCARD_NUMBER_LENGTH_LIMIT = 11;
export const SMARTCARD_NUMBER_LIMIT_PLACEHOLDER = "{{limit}}";

//routes
export const CONFIRM_PAYMENT_ROUTE = "/pages/confirm-payment/confirm-payment";
export const PAYMENT_RESULT_ROUTE = "/pages/payment_result/payment_result";
export const SMARTCARD_DETAILS_ROUTE = "/pages/smartcard-details/smartcard-details";
export const ALTER_PACKAGE_ROUTE = "/pages/alter-package/alter-package";
export const MANAGE_FAVORITES_ROUTE = "/pages/favourites/favourites";
export const PAY_NEW_CARD_ROUTE = "/pages/pay_new_smartcard/pay_new_smartcard";
export const PAY_NEW_SMARTCARD_ROUTE = "/pages/pay_new_smartcard/pay_new_smartcard";
export const PAYMENT_DETAILS_ROUTE = "/pages/payment-details/payment-details";
export const PAYMENT_RECEIPT_ROUTE = "/pages/payment_receipt/payment_receipt";
export const ADD_FAVORITE_ROUTE = "/pages/add-favourite/add-favourite";
export const PAYMENT_HISTORY_ROUTE = "/pages/payments-history/payments-history";
export const INDEX_ROUTE = "/pages/index/index";
export const REPLACE_FAVORITE_ROUTE = "/pages/replace-favourite/replace-favourite";
export const ONBOARDING_ROUTE = "/pages/onboarding/onboarding";


//package groups
export const PACKAGE_GROUPS = [
  "DTH", // Antenna de Grelha
  "DTT" //  Antena Parabolica
];

// date
export const DEFAULT_DATE_PATTERN = 'D MMMM YYYY - H:mm'

//common
export const SERVICE_PROVIDER = "StarTimes";
export const EMPTY_STRING = "";
export const HIPHEN_STRING = "-";
export const ZERO_INT = 0;


// prod configs
export const SHORTCODE                  = '900053'; // Service Provider Shortcode
export const EXTERNAL_API_PDF           = 'https://gateway.m-pesa.vm.co.mz/generic-mini-app-service/v2/documents/pdf'; // Receipts API
export const EXTERNAL_API_BENEFICIARY   = '/service-adapter/v1/generic/proxy/external/BENEFICIARIES'; // Manage favourites API
export const EXTERNAL_API_TRANSACTION   = '/service-adapter/v1/generic/proxy/external/QUERY_PAYMENTS'; // List transactions API
export const EXTERNAL_API_PRE_VAL       = '/service-adapter/v1/generic/proxy/external/PAYMENT_PRE_VAL'; // Payment pre validation API
export const EXTERNAL_API_PAYMENT       = '/service-adapter/v1/generic/proxy/external/PAY_STARTIMES'; // Pay utilitlies (for StarTimes) API
export const EXTERNAL_API_LIST_PRODUCTS = '/service-adapter/v1/generic/proxy/external/STARTIMES_LIST_PRODUCTS'; // fetch package list (for StarTimes) API
export const EXTERNAL_API_TV_PRE_VAL    = '/service-adapter/v1/generic/proxy/external/STARTIMES_METER_PRE_VAL'; // fetch smartCard details (for StarTimes) API


// mock configs
export const MOCK_MSISDN              = '258849903958';
export const MOCK_SHORTCODE           = '900056'; // mock short code
export const MOCK_EXTERNAL_API        = 'https://mpesa-miniapp-mock.onrender.com/api/v1/miniapp' //'https://mpesa-miniapp-mock.onrender.com/api/v1/miniapp'; 
export const MOCK_EXTERNAL_API_TOKEN  = '5e13a0df136e461cab89ae28e154769d'; 

// environemnt configs
export const MOCK_ENV                 = 'mock';
export const PROD_ENV                 = 'prod';
export const ENVIRONMENT              = PROD_ENV; //use this flag to set the environment

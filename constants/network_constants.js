// favorite request commands
export const LIST_BEN = "LIST_BEN" // list favourites
export const PRE_VAL_TV = "PRE_VAL_TV" // get smartcard details 
export const CREATE_BEN = "CREATE_BEN" // create favourite
export const UPDATE_BEN = "UPDATE_BEN" // update favourites
export const DELETE_BEN = "DELETE_BEN" // delete favourite
export const LIST_TRAN = "LIST_TRAN" // list transactions 
export const LIST_TV_PACKAGES = "LIST_TV_PACKAGES" // list packages 
export const PRE_VAL = "PRE_VAL" // payment pre-validation
export const NEW_PAYMENT = "NEW_PAYMENT" // pay utility 
export const DOWNLOAD_RECEIPT = "DOWNLOAD_RECEIPT" // download receipt

// network error codes 
export const ITEM_NOT_FOUND = 204 // Beneficiary does not exist
export const SYSTEM_BUSY = 1000 // G2 System Busy
export const AUTHENTICATION_FAIL = 1001 // Failed Authentication Request due to invalid credentials or session ID
export const INPUT_PARAMETER_METER = 1002 // Missing parameters in the request packet
export const INVALID_PIN = 2001 // Invalid Customer PIN
export const INVALID_SHORTCODE = 2002 // Organization shortcode or receiver is invalid or not found
export const INVALID_AMOUNT = 2003 // Provided timestamp is invalid
export const INVALID_TIMESTAMP = 2005 // Customer does not have sufficient amount in his wallet
export const BALANCE_INSUFICIENT = 2006 // Amount below the minimum amount the current service
export const INSUFFICIENT_FUNDS = 2007 // Amount below the minimum amount the current service
export const INSUFFICIENT_FUNDS_FOR_FEES = 2008 // Amount not suficient to pay fees such as radio tax, garbage tax, etc
export const INVALID_BENEFICIARY_NAME = 2009 // Favorite name invalid, according to the business rules
export const INVALID_BENEFICIARY_REFERENCE = 2010 // Favorite smartcard number invalid, according to the business rules
export const SERVICE_UNAVAILABLE = 2011 // Service unavailable response
export const MSISDN_INVALID = 2051 // Invalid MSISDN provided
export const EXCEEDED_FAVORITES_LIMIT = 2063 // Exceeded the limit of Favourites for a certain service
export const TRANSACTION_NOT_FOUND = 2404 // Transaction not found
export const SYSTEM_INTERNAL_ERROR = -1 // Error on the internal systems either G2 or iPG or thirdparty
export const INVALID_SESSION_ID = -2 // Invalid Session ID response 
export const TRANSACTION_FAILED = -6 // Transaction Payment Failed
export const INVALID_COMMAND = -7 // Request sending invalid command in the request packet
export const REQUEST_TIMEOUT = -8 // A downstream system(s) timed-out
export const BENEFICIARY_REFERENCE_NOT_FOUND = -9 // Favorite smartcard number not found on TV provider side
export const INVALID_AMOUNT_FOR_G2 = -15 // Invalid Amount used, validated by G2
export const INPUT_PARAMETER_MISSING = -20 // Missing parameters credentials on Authentication Request


//local error codes
export const PACKAGE_LIST_EMPTY = -21 
export const PACKAGE_FETCH_ERROR = -22 
export const NETWORK_UNAVAILABLE = -23
export const FAVORITE_NUM_LIMIT = -24

//payload constants
export const STARTIMES_REQUEST_PARAMETER = "STARTIMES";

Component({
  mixins: [],
  data: {
    bottomTextLines: []
  },
  props: {
    topTitle: '',
    topDescription: '',
    bottomText: '',
    buttonText: '',
    isLoading: false,
    value: null,
    onTap: null,
    cardExtraStyle: '',
    topTitleExtraStyle: '',
    topDescriptionExtraStyle: '',
    buttonExtraStyle: '',
    buttonTextExtraStyle: '',
    bottomTextExtraStyle: '',
    buttonExtraStyle: '',
    topRowBackground: '',
    bottomRowBackground: '',
    isSmallCard: false,
    onlyTitle: true
  },
  didMount() {
    const { props } = this
    this.setData({
      bottomTextLines : props.bottomText && props.bottomText.length ?  props.bottomText.split('\\n') : []
    })
  },
  didUpdate() {},
  didUnmount() {},
  methods: {
    onTap(e) {
      if (this.props.onTap) {
        this.props.onTap(e);
      }
    }
  },
});

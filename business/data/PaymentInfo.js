export default class PaymentInfo {
  // string
  smartCardName;
  // string;
  smartCardNumber;
  // dayJs
  createDate;
  // string
  createTime;
  // string
  clientName;
  // string
  serviceFee;
  // string
  subscription;
  // string
  duration;
  // string
  priceOption;
  // string
  packageCode;
  // string
  receiptNumber;
  // string
  transactionId;
  // object
  data;
}

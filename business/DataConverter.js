import dayjs from 'dayjs';

import PaymentInfo from "./data/PaymentInfo"

import PT from 'dayjs/locale/pt'
import { formatCurrency, formatDateToLocalStandards } from '/utils/utils';

import { EMPTY_STRING } from '/constants/app_constants';


// Set the Portuguese (Portugal) locale globally for dayjs
dayjs.locale(PT);

const app = getApp();

const DataConverter = {

  convertDataToPaymentInfo: (paymentData, favourites) => {
    const payment = new PaymentInfo();
    let favourite = ''
    if(favourites) {
      const foundFavourite = favourites.find((f) => f.reference === paymentData.billreferencenumber)
      if(foundFavourite) {
        favourite = foundFavourite.name
      }
    }

    const dateTime = dayjs(paymentData.timestamp)
    payment.provider = app.serviceProvider;
    payment.smartCardName = favourite;
    payment.clientName = paymentData.trx_data.client_name;
    payment.serviceFee = formatCurrency(paymentData.trx_data.charge_amount,0, app.i18n.common.unavailable);
    payment.subscription = paymentData.trx_data.package_name;
    payment.duration = app.i18n.durationDescription[paymentData.trx_data.price_option];
    payment.smartCardNumber = paymentData.billreferencenumber;
    payment.receiptNumber = paymentData.receipt_number;
    payment.priceOption = paymentData.trx_data.price_option;
    payment.createDate = paymentData.timestamp ? formatDateToLocalStandards(dateTime, app.i18n) : EMPTY_STRING;
    payment.createTime = paymentData.timestamp ? dateTime.format('HH:mm')  : EMPTY_STRING;
    payment.amount = paymentData.amount;
    payment.transactionId = paymentData.trx_data.transaction_id;
    payment.data = paymentData;
    return payment;
  },

  convertDataToPaymentInfoArray: (paymentArrayData, favourites) => {
    return paymentArrayData.map((paymentData) => {
      return DataConverter.convertDataToPaymentInfo(paymentData, favourites);
    });
  },

  createReceiptObject: (data) => {
    const parsedData = {
      meter: data.trx_data.meter,
      token: data.trx_data.recharge,
      energyValue: data.trx_data.token_amount,
      vat: data.trx_data.tax,
      debtPaid: data.trx_data.debt_paid,
      debtAmount: data.trx_data.debt_to_pay,
      garbageFee: data.trx_data.garbage_tax,
      radioFee: data.trx_data.radio_tax,
      amountPaid: data.trx_data.total_paid,
      energyUnit: data.trx_data.energy,
      reference: data.receipt_number,
      createDate: data.timestamp
    };
    
    return parsedData;
  }
};

export default DataConverter;
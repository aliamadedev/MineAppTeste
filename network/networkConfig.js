import {
  MOCK_EXTERNAL_API,
  ENVIRONMENT,
  PROD_ENV,
  EXTERNAL_API_TV_PRE_VAL,
  EXTERNAL_API_BENEFICIARY,
  EXTERNAL_API_TRANSACTION,
  EXTERNAL_API_PRE_VAL,
  EXTERNAL_API_PAYMENT,
  EXTERNAL_API_LIST_PRODUCTS
} from "/constants/config_constants";
import { PRE_VAL_TV, LIST_BEN, CREATE_BEN, UPDATE_BEN, DELETE_BEN, LIST_TRAN, PRE_VAL, NEW_PAYMENT, LIST_TV_PACKAGES } from "/constants/network_constants";
import { logd } from "/utils/utils";

// Default configuration
let config = ENVIRONMENT;

function isProdEnvironment() {
  return config === PROD_ENV;
}

function getAPI(command) {
  logd(`calling ${command} API`);
  switch (command) {
    case LIST_BEN:
    case CREATE_BEN:
    case UPDATE_BEN:
    case DELETE_BEN:
      return config === PROD_ENV ? EXTERNAL_API_BENEFICIARY : MOCK_EXTERNAL_API;
    case PRE_VAL_TV:
      return config === PROD_ENV ? EXTERNAL_API_TV_PRE_VAL : MOCK_EXTERNAL_API;
    case LIST_TRAN:
      return config === PROD_ENV ? EXTERNAL_API_TRANSACTION : MOCK_EXTERNAL_API;
    case PRE_VAL:
      return config === PROD_ENV ? EXTERNAL_API_PRE_VAL : MOCK_EXTERNAL_API;
    case NEW_PAYMENT:
      return config === PROD_ENV ? EXTERNAL_API_PAYMENT : MOCK_EXTERNAL_API;
    case LIST_TV_PACKAGES:
      return config === PROD_ENV ? EXTERNAL_API_LIST_PRODUCTS : MOCK_EXTERNAL_API;
    default: {
      console.error(`Invalid command ${command}`);
      return "-1";
    }
  }
}

export { isProdEnvironment, getAPI };

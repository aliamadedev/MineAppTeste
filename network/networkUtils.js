import { isProdEnvironment } from "/network/networkConfig";
import { loge } from "/utils/utils";
import { MOCK_MSISDN, MOCK_SHORTCODE, SHORTCODE } from "/constants/config_constants";
import {
  LIST_BEN,
  PRE_VAL_TV,
  LIST_TRAN,
  CREATE_BEN,
  UPDATE_BEN,
  DELETE_BEN,
  LIST_TV_PACKAGES,
  PRE_VAL,
  NEW_PAYMENT,
  DOWNLOAD_RECEIPT,
  STARTIMES_REQUEST_PARAMETER
} from "/constants/network_constants";
import uniqid from "uniqid";

/**
 * Creates request payloads based on the request command field
 * @param {string} operation
 * @param {object} data
 */
export const payLoadGenerator = (operation, data = {}) => {
  let requestId = uniqid();
  switch (operation) {
    case LIST_BEN:
      return {
        command: LIST_BEN,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE
      };
    case PRE_VAL_TV:
      return {
        command: PRE_VAL_TV,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
        billreferencenumber: `${data.smartCardNo}`
      };
    case CREATE_BEN:
      return {
        command: CREATE_BEN,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
        beneficiary_name: `${data.beneficiaryName}`,
        beneficiary_reference: `${data.beneficiaryReference}`
      };
    case UPDATE_BEN:
      return {
        command: UPDATE_BEN,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
        beneficiary_name: `${data.beneficiaryName}`,
        beneficiary_reference: `${data.beneficiaryReference}`,
        new_beneficiary_reference: `${data.newBeneficiaryReference}`,
        new_beneficiary_name: `${data.newBeneficiaryName}`,
        update_option: "0"
      };
    case DELETE_BEN:
      return {
        command: DELETE_BEN,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
        beneficiary_name: `${data.beneficiaryName}`,
        beneficiary_reference: `${data.beneficiaryReference}`
      };
    case LIST_TRAN:
      return {
        command: LIST_TRAN,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
        page: data.page
      };
    case LIST_TV_PACKAGES:
      return {
        command: LIST_TV_PACKAGES,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE
      };
    case PRE_VAL:
      return {
        command: PRE_VAL,
        service_channel: "MINI_APP",
        user: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
        billreferencenumber: `${data.smartCardNo}`,
        amount: data.packagePrice,
        request_id: requestId,
        parameters: {
          package_code: `${data.packageCode}`,
          package_price: data.packagePrice,
          price_option: `${data.priceOption}`,
          package_name: `${data.packageName}`
        }
      };
    case NEW_PAYMENT:
      return {
        miniApp: STARTIMES_REQUEST_PARAMETER,
        phoneNumber: isProdEnvironment() ? "${MSISDN}" : MOCK_MSISDN,
        data: {
          amount: data.amount,
          referenceNumber: `${data.smartCardNo}`,
          shortcode: isProdEnvironment() ? SHORTCODE : MOCK_SHORTCODE,
          parameters: {
            package_code: `${data.packageCode}`,
            package_price: data.amount,
            price_option: `${data.priceOption}`,
            package_name: `${data.packageName}`
          }
        }
      };
    case DOWNLOAD_RECEIPT:
      // TODO - update this when the receipts API is ready
      return {
        miniApp: "CREDELEC",
        token: "0322 9579 6081 7683 1560",
        meter: "54243454243",
        vat: 0,
        debtPaid: 0,
        debtAmount: 0,
        radioFee: 0,
        garbageFee: 0,
        amountPaid: 100.5,
        energyValue: 109.8,
        energyUnit: "40 Kwh",
        reference: "AI8709Z729",
        date: "2023-09-08T11:38:37",
        lang: "pt"
      };
    default:
      console.log(operation + " is not a supported option");
      return null;
  }
};

export const handleFailedError = err => {
  try {
    return err.data.httpCode;
  } catch (error) {
    return err.error;
  }
};

/**
 * converts error codes that are type string to a numerical type
 * @param {*} number 
 */
export const standardizeErrorCode = number => {
  // Check if the input is already a number (integer)
  if (typeof number === "number" && Number.isInteger(number)) {
    // If it's already an integer, return it immediately
    return number;
  }

  const convertedInt = parseInt(number);

  if (isNaN(convertedInt)) {
    // If it's NaN, return an error or handle it as needed
    loge("Error: Not a valid integer");
    return -1;
  } else {
    // If it's a valid integer, return the integer
    return convertedInt;
  }
};

import base64 from "base-64";
import { isProdEnvironment, getAPI } from "./networkConfig";
import {
  CREATE_BEN,
  DELETE_BEN,
  PRE_VAL_TV,
  LIST_BEN,
  LIST_TRAN,
  UPDATE_BEN,
  LIST_TV_PACKAGES,
  PRE_VAL,
  NEW_PAYMENT,
  INSUFFICIENT_FUNDS,
  INSUFFICIENT_FUNDS_FOR_FEES,
  PACKAGE_LIST_EMPTY,
  DOWNLOAD_RECEIPT,
  ITEM_NOT_FOUND,
  BENEFICIARY_REFERENCE_NOT_FOUND,
  TRANSACTION_FAILED,
  NETWORK_UNAVAILABLE,
  FAVORITE_NUM_LIMIT,
  PACKAGE_FETCH_ERROR
} from "/constants/network_constants";
import { MOCK_EXTERNAL_API_TOKEN, EXTERNAL_API_PDF } from "/constants/config_constants";
import { handleFailedError, payLoadGenerator, standardizeErrorCode } from "/network/networkUtils";
import pt from "/i18n/pt";
import { EMPTY_STRING } from "/constants/app_constants";
import { logd } from "/utils/utils";

export default function apiCall(command, data, page = null) {
  switch (command) {
    case LIST_BEN:
      return fetchFavourites();
    case PRE_VAL_TV:
      return fetchFavourite(data.smartCardNo, page);
    case CREATE_BEN:
      return addFavourite(data.smartCard, page);
    case UPDATE_BEN:
      return updateFavourite(data.oldSmartCard, data.newSmartCard, data.isReplace, page);
    case DELETE_BEN:
      return deleteFavourite(data.smartCard, page);
    case LIST_TRAN:
      return fetchPayments(data);
    case LIST_TV_PACKAGES:
      return fetchSmartCardPackages();
    case PRE_VAL:
      return fetchPreValidation(data.packageCode, data.packagePrice, data.priceOption, data.packageName, data.smartCardNo, page);
    case NEW_PAYMENT:
      return paySubscription(data.packageName, data.packageCode, data.smartCardNo, data.priceOption, data.amount, page);
    case DOWNLOAD_RECEIPT:
      return downloadReceipt(data.amount, data.packageName, data.receiptNumber, data.smartCardNumber, data.duration, data.paidDate, page);
    default:
      console.error(command + "command not supported");
      return null;
  }
}

// fetch list of favorite smartcards/beneficiaries
function fetchFavourites() {
  const data = payLoadGenerator(LIST_BEN);
  if (isProdEnvironment()) {
    const proxyData = {
      msisdn: "msisdn",
      proxiedRequest: {
        needsIdentity: true,
        needsPIN: false,
        requestInfo: {
          httpMethod: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          payload: JSON.stringify(data)
        }
      }
    };
    return prodApiBaseRequest(LIST_BEN, proxyData, true);
  } else {
    return mockApiBaseRequest(data, true, LIST_BEN);
  }
}

// Get a single favourite smartcard (beneficiary)
function fetchFavourite(smartCardNo, page) {
  return new Promise((resolve, reject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false });
        } else {
          const data = payLoadGenerator(PRE_VAL_TV, { smartCardNo });
          if (isProdEnvironment()) {
            const proxyData = {
              msisdn: "msisdn",
              proxiedRequest: {
                needsIdentity: true,
                needsPIN: false,
                requestInfo: {
                  httpMethod: "POST",
                  headers: {
                    "Content-Type": "application/json"
                  },
                  payload: JSON.stringify(data)
                }
              }
            };
            resolve(prodApiBaseRequest(PRE_VAL_TV, proxyData, true));
          } else {
            resolve(mockApiBaseRequest(data, true, PRE_VAL_TV));
          }
        }
      }
    });
  });
}

// Create a single favorite smartcard (beneficiary)
function addFavourite(smartCard, page) {
  return new Promise((resolve, reject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false });
        } else {
          const payLoad = {
            beneficiaryName: `${smartCard.name}`,
            beneficiaryReference: `${smartCard.reference}`
          };

          const data = payLoadGenerator(CREATE_BEN, payLoad);

          if (isProdEnvironment()) {
            const proxyData = {
              msisdn: "msisdn",
              proxiedRequest: {
                needsIdentity: true,
                needsPIN: false,
                requestInfo: {
                  httpMethod: "POST",
                  headers: {
                    "Content-Type": "application/json"
                  },
                  payload: JSON.stringify(data)
                }
              }
            };

            const loadingText = pt.addFavourite.addFavouriteLoading;

            resolve(prodApiBaseRequest(CREATE_BEN, proxyData, false, `{"loader": {"text": "${loadingText}"}}`));
          } else {
            resolve(mockApiBaseRequest(data, false, CREATE_BEN));
          }
        }
      }
    });
  });
}

// Update user's favorite smartcard (beneficiary)
function updateFavourite(oldSmartCard, newSmartCard, isReplace = false, page) {
  return new Promise((resolve, reject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false, isUpdateOperation: true });
        } else {
          const payLoad = {
            beneficiaryName: `${oldSmartCard.name}`,
            beneficiaryReference: `${oldSmartCard.reference}`,
            newBeneficiaryReference: `${newSmartCard.reference}`,
            newBeneficiaryName: `${newSmartCard.name}`,
            updateOption: "0"
          };

          const data = payLoadGenerator(UPDATE_BEN, payLoad);

          if (isProdEnvironment()) {
            const proxyData = {
              msisdn: "msisdn",
              proxiedRequest: {
                needsIdentity: true,
                needsPIN: false,
                requestInfo: {
                  httpMethod: "POST",
                  headers: {
                    "Content-Type": "application/json"
                  },
                  payload: JSON.stringify(data)
                }
              }
            };

            const loadingText = isReplace ? pt.replaceFavourite.replaceFavouriteLoading : pt.editFavourite.editFavouriteLoading;

            resolve(prodApiBaseRequest(UPDATE_BEN, proxyData, false, `{"loader": {"text": "${loadingText}"}}`));
          } else {
            resolve(mockApiBaseRequest(data, false, UPDATE_BEN));
          }
        }
      }
    });
  });
}

//  Deletes a favorite smartcard (beneficiary)
function deleteFavourite(smartCard, page) {
  return new Promise((resolve, reject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false, isDeleteOperation: true });
        } else {
          const payLoad = {
            beneficiaryName: `${smartCard.name}`,
            beneficiaryReference: `${smartCard.reference}`
          };

          const data = payLoadGenerator(DELETE_BEN, payLoad);

          if (isProdEnvironment()) {
            let proxyData = {
              msisdn: "msisdn",
              proxiedRequest: {
                needsIdentity: true,
                needsPIN: false,
                requestInfo: {
                  httpMethod: "POST",
                  headers: {
                    "Content-Type": "application/json"
                  },
                  payload: JSON.stringify(data)
                }
              }
            };

            const loadingText = pt.favourites.removeFavouriteLoading;

            resolve(prodApiBaseRequest(DELETE_BEN, proxyData, false, `{"loader": {"text": "${loadingText}"}}`));
          } else {
            resolve(mockApiBaseRequest(data, false, DELETE_BEN));
          }
        }
      }
    });
  });
}

/// fetch list of payments
function fetchPayments(data) {
  const payload = payLoadGenerator(LIST_TRAN, data);
  if (isProdEnvironment()) {
    const proxyData = {
      msisdn: "msisdn",
      proxiedRequest: {
        needsIdentity: true,
        needsPIN: false,
        requestInfo: {
          httpMethod: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          payload: JSON.stringify(payload)
        }
      }
    };
    return prodApiBaseRequest(LIST_TRAN, proxyData, true);
  } else {
    return mockApiBaseRequest(payload, true, LIST_TRAN);
  }
}
// fetch all packages
function fetchSmartCardPackages() {
  const payload = payLoadGenerator(LIST_TV_PACKAGES);
  if (isProdEnvironment()) {
    const proxyData = {
      msisdn: "msisdn",
      proxiedRequest: {
        needsIdentity: true,
        needsPIN: false,
        requestInfo: {
          httpMethod: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          payload: JSON.stringify(payload)
        }
      }
    };
    return prodApiBaseRequest(LIST_TV_PACKAGES, proxyData, true);
  } else {
    return mockApiBaseRequest(payload, true, LIST_TV_PACKAGES);
  }
}

function fetchPreValidation(packageCode, packagePrice, priceOption, packageName, smartCardNo, page) {
  return new Promise((resolve, reject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false });
        } else {
          const payload = payLoadGenerator(PRE_VAL, { packageCode, packagePrice, priceOption, packageName, smartCardNo });

          if (isProdEnvironment()) {
            const proxyData = {
              msisdn: "msisdn",
              proxiedRequest: {
                needsIdentity: true,
                needsPIN: false,
                requestInfo: {
                  httpMethod: "POST",
                  headers: {
                    "Content-Type": "application/json"
                  },
                  payload: JSON.stringify(payload)
                }
              }
            };
            resolve(prodApiBaseRequest(PRE_VAL, proxyData, true));
          } else {
            resolve(mockApiBaseRequest(payload, true, PRE_VAL));
          }
        }
      }
    });
  });
}

function paySubscription(packageName, packageCode, smartCardNo, priceOption, amount, page) {
  return new Promise((resolve, reject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false });
        } else {
          const payload = payLoadGenerator(NEW_PAYMENT, { packageName, packageCode, smartCardNo, priceOption, amount });

          if (isProdEnvironment()) {
            const proxyData = {
              msisdn: "msisdn",
              pin: {
                algorithm: "RSA",
                data: "pin"
              },
              proxiedRequest: {
                needsIdentity: true,
                needsPIN: true,
                requestInfo: {
                  httpMethod: "POST",
                  headers: {
                    "Content-Type": "application/json",
                    "Accept-Language": "pt"
                  },
                  payload: JSON.stringify(payload)
                }
              }
            };

            const loadingText = pt.makePayment.makePaymentLoading;

            resolve(prodApiBaseRequest(NEW_PAYMENT, proxyData, true, `{"loader": {"text": "${loadingText}"}}`, ["msisdn", "pin"]));
          } else {
            resolve(mockApiBaseRequest(payload, true, NEW_PAYMENT));
          }
        }
      }
    });
  });
}

function downloadReceipt(amount, packageName, receiptNumber, smartCardNumber, duration, paidDate, page) {
  return new Promise((outerResolve, outerReject) => {
    my.getNetworkType({
      success: res => {
        if (!res.networkAvailable) {
          errorHandler(NETWORK_UNAVAILABLE, page, { fromSelectedFavorite: false, showCancel: false });
        } else {
          const payload = payLoadGenerator(DOWNLOAD_RECEIPT, { amount, packageName, receiptNumber, smartCardNumber, duration, paidDate });

          const urlParams = `data=${base64.encode(JSON.stringify(payload))}`;
          logd(urlParams);
          outerResolve(
            new Promise((resolve, reject) => {
              my.downloadFile({
                url: `${EXTERNAL_API_PDF}?${urlParams}`,
                header: {
                  "Content-Type": "application/pdf"
                },
                success: ({ apFilePath }) => {
                  my.openDocument({
                    filePath: apFilePath,
                    fileType: "pdf",
                    success: res => {
                      resolve(res);
                    },
                    fail: err => {
                      reject({
                        error: err.error,
                        errorMessage: "Error while trying to open pdf preview: " + err.errorMessage
                      });
                    }
                  });
                },
                fail: err => {
                  reject({
                    error: err.error,
                    errorMessage: "Error while trying to download pdf preview: " + err.errorMessage
                  });
                }
              });
            })
          );
        }
      }
    });
  });
}

/**
 *  Mock base request
 * (Note that mocked API requests dont show the full screen loading)
 * @param {*} data
 * @param {*} content
 */
function mockApiBaseRequest(data, content = false, command = EMPTY_STRING) {
  return new Promise((resolve, reject) => {
    my.request({
      url: getAPI(command),
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: MOCK_EXTERNAL_API_TOKEN,
        "Accept-Language": "pt"
      },
      data: JSON.stringify(data),
      success: function(res) {
        const data = res.data;
        if (res.status == 200) {
          // Protect data content when it's not json like
          try {
            const { response_code, response_desc } = data;

            if (response_code == 0) {
              if (content) {
                resolve(data);
              } else {
                resolve(true);
              }
            } else {
              reject({
                error: response_code,
                errorMessage: response_desc
              });
            }
          } catch (e) {
            console.log("Request timeout");
            reject({
              error: data.httpCode,
              errorMessage: res.proxiedResponse.status
            });
          }
        } else {
          // TODO: Throw exception with message
          console.error(res);
          reject({
            error: data.httpCode,
            errorMessage: res.proxiedResponse.status
          });
        }
      },
      fail: function(err) {
        console.error(err);
        reject({
          error: handleFailedError(err)
        });
      }
    });
  });
}

// production base request
function prodApiBaseRequest(command = EMPTY_STRING, proxyData, forContent = false, configOptions = "{}", replaceParams = ["msisdn"]) {
  return new Promise((resolve, reject) => {
    my.call("makeRequest", {
      replaceParams: replaceParams,
      configOptions: configOptions,
      payload: JSON.stringify(proxyData),
      url: getAPI(command),
      success: function(res) {
        const data = res.proxiedResponse.data;
        if (data.httpCode == 200) {
          // Protect data content when it's not json like
          try {
            const content = JSON.parse(data.content);
            const { response_code, response_desc } = content;

            if (response_code == 0) {
              if (forContent) {
                resolve(content);
              } else {
                resolve(true);
              }
            } else {
              console.log(res);
              reject({
                error: response_code,
                errorMessage: response_desc
              });
            }
          } catch (e) {
            console.log("Request timeout");
            reject({
              error: data.httpCode,
              errorMessage: res.proxiedResponse.status
            });
          }
        } else {
          // TODO: Throw exception with message
          console.log(res);
          reject({
            error: data.httpCode,
            errorMessage: res.proxiedResponse.status
          });
        }
      },
      fail: function(err) {
        console.log(err);
        reject({
          error: handleFailedError(err)
        });
      }
    });
  });
}

/**
 * errorHandler: Checks the error code from request response and throws the error accordingly
 * @param {number} errorCode : Error code received from server
 * @param {page} page : Page object instance
 */
export function errorHandler(errorCode, page, config = { fromSelectedFavorite: true, showCancel: true, isDeleteOperation: false, isUpdateOperation: false }) {
  switch (standardizeErrorCode(errorCode)) {
    case INSUFFICIENT_FUNDS:
    case INSUFFICIENT_FUNDS_FOR_FEES:
      showError(
        "/assets/icons/si_no_balance.svg",
        pt.error.insufficientBalanceTitle,
        pt.error.insufficientBalanceDescription,
        pt.common.btnRetry,
        "retry",
        config.fromSelectedFavorite ? EMPTY_STRING : pt.common.btnCancel,
        config.fromSelectedFavorite ? "closeModal" : "cancelPayment",
        page
      );
      break;
    case BENEFICIARY_REFERENCE_NOT_FOUND:
    case ITEM_NOT_FOUND:
      showError(
        "/assets/icons/hi_warning.svg",
        pt.error.invalidSmartCardNumber,
        pt.error.invalidSmartCardNumberDescription,
        config.fromSelectedFavorite ? pt.common.btnHome : pt.common.btnRetry,
        "closeModal",
        config.fromSelectedFavorite ? EMPTY_STRING : pt.common.btnCancel,
        config.fromSelectedFavorite ? "closeModal" : "cancelPayment",
        page
      );
      break;
    case TRANSACTION_FAILED:
      showError(
        "/assets/icons/hi_frown.svg",
        pt.error.transactionFailedTitle,
        pt.error.transactionFailedDescription,
        config.fromSelectedFavorite ? pt.common.btnHome : pt.common.btnRetry,
        "retry",
        config.fromSelectedFavorite ? EMPTY_STRING : pt.common.btnCancel,
        config.fromSelectedFavorite ? "closeModal" : "cancelPayment",
        page
      );
      break;
    case PACKAGE_LIST_EMPTY:
      showError(
        "/assets/icons/hi_warning.svg",
        pt.common.genericErrorTitle,
        pt.common.genericErrorMessage,
        config.fromSelectedFavorite ? pt.common.btnHome : pt.common.btnRetry,
        "closeModal",
        config.fromSelectedFavorite ? EMPTY_STRING : pt.common.btnCancel,
        config.fromSelectedFavorite ? "closeModal" : "cancelPayment",
        page
      );
      break;
    case NETWORK_UNAVAILABLE:
      showError(
        "/assets/icons/hi_network.svg",
        pt.error.networkErrorTitle,
        pt.error.networkErrorDescription,
        config.fromSelectedFavorite ? pt.common.btnHome : pt.common.btnRetry,
        "retry",
        config.showCancel ? (config.fromSelectedFavorite ? EMPTY_STRING : pt.common.btnCancel) : null,
        config.showCancel ? (config.fromSelectedFavorite ? "retry" : "cancelOperation") : null,
        page
      );
      if (config.isDeleteOperation || config.isUpdateOperation) {
        var alertModal = page.data.alertModal;
        alertModal.primaryButtonLabel = pt.common.btnRetry;
        alertModal.primaryButtonOnTap = "retry";
        page.setData({ isLoading: false });
        page.setData({ alertModal: alertModal });
      }
      break;
    case FAVORITE_NUM_LIMIT:
      showError(
        "/assets/icons/hi_warning.svg",
        pt.alerts.favouriteLimit.title,
        pt.alerts.favouriteLimit.body1,
        pt.alerts.favouriteLimit.primaryBtn,
        "onConfirmReplace",
        pt.alerts.favouriteLimit.secondaryBtn,
        "onCancelModal",
        page
      );
      break;
    case PACKAGE_FETCH_ERROR:
      showError(
        "/assets/icons/hi_warning.svg",
        pt.common.genericErrorTitle,
        pt.common.genericErrorMessage,
        pt.common.btnRetry,
        "retry",
        pt.common.btnCancel,
        "onNoPackagesCancel",
        page
      );
      break;
    default:
      showError(
        "/assets/icons/hi_warning.svg",
        pt.common.genericErrorTitle,
        pt.common.genericErrorMessage,
        config.fromSelectedFavorite ? pt.common.btnHome : pt.common.btnRetry,
        "closeModal",
        config.fromSelectedFavorite ? EMPTY_STRING : pt.common.btnCancel,
        config.fromSelectedFavorite ? "closeModal" : "cancelPayment",
        page
      );
      break;
  }
}

function showError(
  image = "/assets/icons/hi_warning.svg",
  error,
  errorMsg,
  primaryBtnLbl = pt.common.btnRetry,
  primaryBtnFunction = "closeModal",
  secondaryBtnLbl = pt.common.btnCancel,
  secondaryBtnFunction = "closeModal",
  page
) {
  showAlert(image, error, errorMsg, primaryBtnLbl, primaryBtnFunction, secondaryBtnLbl, secondaryBtnFunction, page);
}

function showAlert(image, title, body1, primaryBtnLbl, primaryBtnOnTap, secondaryBtnLbl, secondaryBtnOnTap, page) {
  closeModal(page);
  page.setData({
    alertModal: {
      image: image,
      title: title,
      body1: body1,
      primaryBtnLbl: primaryBtnLbl,
      primaryBtnOnTap: primaryBtnOnTap,
      secondaryBtnLbl: secondaryBtnLbl,
      secondaryBtnOnTap: secondaryBtnOnTap
    }
  });
  page.modalRef.show();
}

function closeModal(page) {
  // Check modal is visible
  if (page.modalRef.isVisible()) {
    page.modalRef.hide();
    page.setData({
      alertModal: {
        image: "",
        title: "",
        body1: "",
        primaryBtnLbl: "",
        primaryBtnOnTap: null,
        secondaryBtnLbl: null,
        secondaryBtnOnTap: null
      }
    });
  }
}

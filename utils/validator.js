import { SMARTCARD_NUMBER_LENGTH_LIMIT, SMARTCARD_NUMBER_LIMIT_PLACEHOLDER } from "/constants/app_constants";

/**
 * Validate smartCard name
 *
 * It returns error message if it is not complaint otherwise it returns null.
 * @param smartCardName {string} smartCard name value
 * @param i18n {Object} internationalization map object
 * @example
 * smartCardNameValidator('Mom House', i18n)
 */
const smartCardNameValidator = function(smartCardName, i18n) {
  if (smartCardName === "") {
    return null;
  }

  // Regular expression pattern to validate a string with alphanumeric and with length between 0 and 20
  // NOTE: 0 character as minimum just to not display error when start typing yet was the validation event is on-type event.
  let pattern = /^[A-Za-z0-9\s]{0,20}$/;

  const regex = new RegExp(pattern);

  // Test the smartCardName against the regular expression
  const isValid = regex.test(smartCardName);

  if (!isValid) {
    return i18n.error.invalidSmartCardName;
  }

  return null;
};

/**
 * Validate SmartCard number
 *
 * It returns error message if it is not complaint otherwise it returns null.
 * @param smartCardNo {string} smartCard number value
 * @param i18n {Object} internationalization map object
 * @example
 * smartCardNoValidate('2348758093723', i18n)
 */
const smartCardNoValidator = function(smartCardNo, i18n) {
  if (smartCardNo === "") {
    return null;
  }

  // Regular expression pattern to check only digits
  let pattern = /^\d+$/;

  const regex = new RegExp(pattern);

  // Test the smartCardNo against the regular expression
  const isValid = regex.test(smartCardNo);

  if (!isValid) {
    return i18n.error.invalidSmartCardNumber;
  }

  if (smartCardNo.length < SMARTCARD_NUMBER_LENGTH_LIMIT) {
    return i18n.error.incompleteSmartCardNumber.replace(SMARTCARD_NUMBER_LIMIT_PLACEHOLDER, SMARTCARD_NUMBER_LENGTH_LIMIT);
  }

  if (smartCardNo.length > SMARTCARD_NUMBER_LENGTH_LIMIT) {
    return i18n.error.invalidSmartCardNumber;
  }

  return null;
};

export { smartCardNameValidator, smartCardNoValidator };

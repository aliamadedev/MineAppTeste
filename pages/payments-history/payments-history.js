import DataConverter from "/business/DataConverter";
import { EMPTY_STRING, PAYMENT_DETAILS_ROUTE } from "/constants/app_constants";
import { formatCurrency, groupArrayValues, routeBuilder } from "/utils/utils";

const app = getApp();

Page({
  data: {
    isLoading: true,
    sections: [],
    pageTotalResults: 5,
    isLoadingOnScroll: false,
    currentPage: 1,
    payments: []
  },
  onLoad() {
    this.setData({ i18n: app.i18n });
  },
  onShow() {
    this.showData();
  },

  async showData(){
    let totalCalls = 3;
    while(totalCalls >= 1) {
      if(this.canFetch()){
        await this.fetchPaymentHistory(this.data.currentPage);
        this.data.currentPage++;
      }
      totalCalls--;
    }
    this.initialize();
  },

  async initialize() {
    const payments = DataConverter.convertDataToPaymentInfoArray(this.data.payments, app.favouriteSmartCards);
    const groupedPayments = groupArrayValues(payments, (paymentA, paymentB) => {
      return paymentA.createDate === paymentB.createDate;
    });

    const listSections = [];
    let sectionTitle;
    let sectionData;

    groupedPayments.forEach((group) => {

      sectionTitle = group[0].createDate ? group[0].createDate : EMPTY_STRING;
      sectionData = [];

      group.forEach((paymentInfo) => {
        sectionData.push({
          leftTitle: paymentInfo.smartCardName,
          leftDescription: paymentInfo.smartCardNumber,
          rightTitle: formatCurrency(paymentInfo.amount, 0, app.i18n.common.unavailable),
          rightDescription: paymentInfo.subscription,
          onRowClick: (paymentInfo) => {
            app.navigateToPayload = {paymentInfo, data: DataConverter.createReceiptObject(paymentInfo.data)};
            my.navigateTo({ url: routeBuilder(PAYMENT_DETAILS_ROUTE, { canRepay: true }) });
          },
          rowData: paymentInfo
        });
      });

      listSections.push({
        title: sectionTitle,
        data: sectionData,
      });
    });

    this.setData({ sections: listSections, isLoading: false });
  },

  async scrollToLower() {
    if (this.canFetch()) {
      this.setData({isLoadingOnScroll: true });
      await this.fetchPaymentHistory(this.data.currentPage);
      this.initialize();
      this.data.currentPage++;
    }
  },

  canFetch(){
    return this.data.pageTotalResults !== 0 || this.data.pageTotalResults >= 5;
  },

  async fetchPaymentHistory(page) {
    try {
      // Fetch customer Payment History OR Transactions
      const payments = await app.fetchPaymentsHistory(page);
      const allPayments = [...this.data.payments, ...payments.data];

      this.setData(
        { payments: allPayments, 
          isLoadingOnScroll: false,
          pageTotalResults: payments.data.length 
        });
    } catch (err) {
      this.setData({ isLoadingOnScroll: false, showToast: true, toastContent: this.data.i18n.common.genericErrorToast, toastType: 'warning' });
      console.log(err);
    }
  }

});

import DataConverter from "/business/DataConverter";
import { EMPTY_STRING, HIPHEN_STRING, INDEX_ROUTE, PAYMENT_RESULT_ROUTE } from "/constants/app_constants";
import { errorHandler } from "/network/networkManager";
import { formatCurrency, routeBuilder } from "/utils/utils";

const app = getApp();

Page({
  data: {
    isLoading: false,
    shortcode: app.globalData.shortcode,
    packageName: EMPTY_STRING,
    smartCardName: EMPTY_STRING,
    smartCardNo: EMPTY_STRING,
    amount: undefined,
    months: undefined,
    displayAmount: undefined
  },
  onLoad(query) {
    const { priceOption, smartCardNo, packageName, amount, packageCode, customerName } = query;

    this.setData({
      displayAmount: formatCurrency(amount, 2, app.i18n.common.unavailable),
      amount: parseInt(amount),
      i18n: app.i18n,
      customerName,
      priceOption,
      packageCode,
      smartCardNo,
      packageName: packageName || HIPHEN_STRING,
      duration: app.i18n.durationDescription[priceOption] || HIPHEN_STRING,
      serviceFee: formatCurrency(app.navigateToPayload.serviceFee, 2, app.i18n.common.unavailable)  
    });

    if (smartCardNo && packageName) {
      this.setData({
        smartCardNo,
        packageName,
        amount
      });
    } else if (smartCardNo) {
      const smartCard = app.favouriteSmartCards.find(favouriteSmartCard => favouriteSmartCard.reference === smartCardNo);
      this.setData({
        smartCardNo,
        smartCardName: smartCard.name,
        packageName: smartCard.package_name,
        amount: amount
      });
    } else if (app.navigateBackPayload) {
      this.setData({
        smartCardName: app.navigateBackPayload.smartCardName,
        smartCardNo: app.navigateBackPayload.smartCardNo,
        amount: amount
      });
      app.navigateBackPayload = null;
    }
  },
  onShow() {},
  // Methods
  async onConfirm() {
    // Close modal if visible
    this.closeModal();
    this.setData({
      isLoading: true
    });
    const { priceOption, smartCardNo, packageName, amount, packageCode } = this.data;

    try {
      let response = await app.paySubscription(packageName, packageCode, smartCardNo, priceOption, amount, this);

      response = {
        ...response,
        createDate: response.timestamp
      };

      app.navigateToPayload = DataConverter.convertDataToPaymentInfo(response.data);
      app.favouriteSmartCardsDetailed = [...app.favouriteSmartCardsDetailed].map(smartCard => {
        if (smartCard.smartcard_number === smartCardNo) {
          smartCard.package_name = response.trx_data.package_name;
          smartCard.package_code = response.trx_data.package_code;
        }
        return smartCard;
      });
      my.reLaunch({
        url: routeBuilder(PAYMENT_RESULT_ROUTE, { smartCardNo: smartCardNo, amount: amount })
      });
    } catch (err) {
      console.error(err);
      errorHandler(err.error, this, { fromSelectedFavorite: false });
    } finally {
      this.setData({
        isLoading: false
      });
    }
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  closeModal() {
    // Check modal is visible
    if (this.modalRef.isVisible()) {
      this.modalRef.hide();
      this.setData({
        alertModal: {
          image: EMPTY_STRING,
          title: EMPTY_STRING,
          body1: EMPTY_STRING,
          primaryBtnLbl: EMPTY_STRING,
          primaryBtnOnTap: null,
          secondaryBtnLbl: null,
          secondaryBtnOnTap: null
        }
      });
    }
  },
  cancelPayment() {
    my.reLaunch({
      url: INDEX_ROUTE
    });
  },
  retry() {
    my.navigateBack();
  },

  showAlert(title, body1, primaryBtnLbl, primaryBtnOnTap, secondaryBtnLbl, secondaryBtnOnTap, image) {
    this.closeModal();
    this.setData({
      alertModal: {
        image: image,
        title: title,
        body1: body1,
        primaryBtnLbl: primaryBtnLbl,
        primaryBtnOnTap: primaryBtnOnTap,
        secondaryBtnLbl: secondaryBtnLbl,
        secondaryBtnOnTap: secondaryBtnOnTap
      }
    });
    this.modalRef.show();
  },
  showError(
    error,
    errorMsg,
    primaryBtnFunction = "closeModal",
    primaryBtnLbl = this.data.i18n.common.btnRetry,
    secondaryBtnLbl = this.data.i18n.common.btnCancel,
    secondaryBtnFunction = "closeModal",
    image = "/assets/icons/hi_warning.svg"
  ) {
    this.showAlert(error, errorMsg, primaryBtnLbl, primaryBtnFunction, secondaryBtnLbl, secondaryBtnFunction, image);
  }
});

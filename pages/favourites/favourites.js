const app = getApp();

Page({
  data: {
    showToast: false,
    toastContent: "",
    toastType: "success",
    selectedFav: null,
    selectFav: false,
    isLoading: false
  },
  onLoad(query) {
    const { selectFav, smartCardNo, smartCardName } = query;
    // Check if there are any selected favourite
    if (smartCardNo && smartCardName) this.setData({ selectedFav: { name: smartCardName, reference: smartCardNo } });

    if (this.data.selectFav != selectFav) this.setData({ selectFav: selectFav || false });

    this.setData({ i18n: app.i18n });
  },
  onShow() {
    this.setData({ favouriteSmartCards: app.favouriteSmartCards, showToast: app.showToast, toastContent: app.toastContent, toastType: app.toastType, isLoadingSmartCards: false });
  },
  onHide() {
    this.onToastClose();
  },
  events: {
    onBack() {
      this.onToastClose();
    }
  },
  // Methods
  goToPayment(e) {
    const smartCard = e.target.dataset.value;
    app.navigateBackPayload = smartCard;
    my.navigateBack();
  },
  // Methods
  goAddFavourite() {
    my.navigateTo({ url: "/pages/add-favourite/add-favourite" });
  },
  goEditFavourite() {
    this.closeSheet();
    my.navigateTo({ url: `/pages/edit-favourite/edit-favourite?ref=${this.data.selectedFav.reference}` });
  },
  goConfirmRemoveFav() {
    let i18nAlertsConfirmFavouriteDelete = this.data.i18n.alerts.confirmFavouriteDelete;
    this.showAlert(
      i18nAlertsConfirmFavouriteDelete.title,
      i18nAlertsConfirmFavouriteDelete.body1,
      this.data.selectedFav.name,
      i18nAlertsConfirmFavouriteDelete.primaryButton,
      "onConfirmRemove",
      i18nAlertsConfirmFavouriteDelete.secondaryButton,
      "onCancelConfirmRev",
      "/assets/icons/hi_idea.svg"
    );
  },
  async onConfirmRemove() {
    this.closeSheet();
    this.setData({ isLoading: true });
    try {
      await app.deleteFavouriteSmartCard(this.data.selectedFav, this);

      const ref = this.data.selectedFav.reference;
      // Create new favorite list without deleted one
      let favouriteSmartCards = [...app.favouriteSmartCards].filter(smartCard => smartCard.reference !== ref).sort((a, b) => a.name.localeCompare(b.name));

      app.favouriteSmartCards = favouriteSmartCards;
      this.setData({ favouriteSmartCards: favouriteSmartCards });
      app.setToastProps(this.data.i18n.favourites.favouriteRemoveSuccess, "success");
    } catch (err) {
      app.setToastProps(this.data.i18n.favourites.favouriteRemoveError, "warning");
      this.setData({ isLoading: true });
    } finally {
      this.closeModal();
      this.setData({ showToast: app.showToast, toastContent: app.toastContent, toastType: app.toastType, isLoading: false });
    }
  },
  onCancelConfirmRev() {
    this.closeModal();
  },
  // Sheet Events
  saveSheetRef(ref) {
    this.sheetRef = ref;
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  openSheet(e) {
    const smartCard = e.target.dataset.value;
    this.sheetRef.show();

    this.setData({ selectedFav: smartCard });
  },
  closeSheet() {
    // If is visible
    if (this.sheetRef.data.visible) {
      this.sheetRef.hide();
    }
  },
  closeModal() {
    // Check modal is visible
    if (this.modalRef.isVisible()) {
      this.modalRef.hide();
      this.setData({
        alertModal: {
          image: "",
          title: "",
          body1: "",
          name1: "",
          primaryButtonLabel: "",
          primaryButtonOnTap: null,
          secondaryButtonLabel: null,
          secondaryButtonOnTap: null
        }
      });
    }
  },
  showAlert(title, body1, name1, primaryButtonLabel, primaryButtonOnTap, secondaryButtonLabel, secondaryButtonOnTap, image) {
    this.closeModal();
    this.setData({
      alertModal: {
        image: image,
        title: title,
        body1: body1,
        name1: name1,
        primaryButtonLabel: primaryButtonLabel,
        primaryButtonOnTap: primaryButtonOnTap,
        secondaryButtonLabel: secondaryButtonLabel,
        secondaryButtonOnTap: secondaryButtonOnTap
      }
    });
    this.modalRef.show();
  },
  openToast() {
    app.showToast = true;
    this.setData({ showToast: true });
  },
  onToastClose() {
    app.showToast = false;
    app.resetToastProps();
    this.setData({ showToast: false });
  },
  retry() {
    this.setData({ isLoading: false });
    app.closeModal(this);
  },
});

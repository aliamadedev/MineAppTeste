import { CONFIRM_PAYMENT_ROUTE, EMPTY_STRING, HIPHEN_STRING, INDEX_ROUTE, PACKAGE_GROUPS } from "/constants/app_constants";
import { errorHandler } from "/network/networkManager";
import { formatCurrency, logd, routeBuilder } from "/utils/utils";

const app = getApp();

Page({
  data: {
    smartCardNo: "",
    smartCardPackageName: "",
    customerName: "",
    isLoadingSmartCards: true,
    isLoadingSmartCardDurations: true,
    showToast: false,
    toastContent: "",
    toastType: "success",
    selectedSmartCardPackage: {},
    packageMap: undefined,
    selectedTab: 0,
    tabs: [{ title: app.i18n.alterPackage.grill.tabTitle }, { title: app.i18n.alterPackage.dish.tabTitle }],
    fromSelectedFavorite: false
  },
  onLoad(query) {
    const { smartCardNo, customerName, packageCode } = query;

    this.setData({
      i18n: app.i18n,
      smartCardNo: smartCardNo
    });
    if (smartCardNo) {
      const smartCard = this.getSmartCard(smartCardNo, query);

      const passedSmartCardPackage = smartCard != undefined ? smartCard.package_code : packageCode;

      const currentSmartCardPackage = app.smartCardPackages.find(smartCardPackage => smartCardPackage.name === passedSmartCardPackage) || {};
      this.setData({
        ...this.data,
        smartCard,
        packageMap: this.groupPackagesByPackageGroup(app.smartCardPackages),
        selectedSmartCardPackage: currentSmartCardPackage,
        currentSmartCardPackage: currentSmartCardPackage,
        customerName: customerName || EMPTY_STRING
      });

      this.setTabData(this.data.selectedTab);
    } else if (app.navigateBackPayload) {
      this.setData({
        ...this.data,
        smartCardName: app.navigateBackPayload.smartCardName,
        smartCardNo: app.navigateBackPayload.smartCardNo
      });
      app.navigateBackPayload = null;
    }
  },
  onShow() {
    this.setData({
      isLoading: false,
      carouselCurrentIdx: 0,
      isLoadingPaymentPrevalidation: false
    });
  },
  onToastClose() {
    app.showToast = false;
    app.resetToastProps();
    this.setData({
      showToast: false
    });
  },
  async onTapContinue(data) {
    try {
      const res = await app.fetchPreValidation(data.packageCode, data.packagePrice, data.priceOption, data.packageName, this.data.smartCardNo, this);
      const serviceFee = res.data.charge_amount;
      app.navigateToPayload = { serviceFee };

      my.navigateTo({
        url: routeBuilder(CONFIRM_PAYMENT_ROUTE, {
          smartCardNo: this.data.smartCardNo,
          amount: data.packagePrice,
          packageCode: data.packageCode,
          priceOption: data.priceOption,
          packageName: data.packageName,
          customerName: this.data.customerName
        })
      });
    } catch (err) {
      console.error(err);
      this.setData({
        isLoadingPaymentPrevalidation: false
      });
      errorHandler(err.error, this, {
        fromSelectedFavorite: this.data.fromSelectedFavorite
      });
    }
  },
  getSmartCard(smartCardNo, query) {
    const smartCard = app.favouriteSmartCards.find(favouriteSmartCard => favouriteSmartCard.reference === smartCardNo);

    if (smartCard === undefined) {
      return { name: null, reference: query.smartCardNo, package_name: query.smartCardPackageName, package_code: query.packageCode };
    }

    return smartCard;
  },

  async fetchFavouriteSmartCard(smartCardNo) {
    try {
      let favouriteSmartCardDetails = app.favouriteSmartCardsDetailed.find(smartCard => smartCard.smartcard_number === smartCardNo);
      if (!favouriteSmartCardDetails) {
        const res = await app.fetchFavouriteSmartCard(smartCardNo, this);
        favouriteSmartCardDetails = res.data;
        app.favouriteSmartCardsDetailed.push(favouriteSmartCardDetails);
      }
      const dueDate = new Date(favouriteSmartCardDetails.due_date);
      this.setData({
        customerName: favouriteSmartCardDetails.client_name,
        amount: favouriteSmartCardDetails.amount,
        displayAmount: formatCurrency(favouriteSmartCardDetails.amount, 0, app.i18n.common.unavailable),
        dueDate: `${dueDate.getDate()} ${this.data.i18n.monthMap[dueDate.getMonth().toString()]} ${dueDate.getFullYear()}`,
        months: favouriteSmartCardDetails.months,
        isLoadingSmartCards: false
      });
    } catch (err) {
      this.setData({
        isLoadingSmartCards: false,
        showToast: true,
        toastContent: this.data.i18n.common.genericErrorToast,
        toastType: "warning"
      });
      console.error(err);
    }
  },

  /**
   * manageCarouselIdx: Set the current active card position
   * @param { number } e
   */
  manageCarouselIdx(e) {
    this.setData({ carouselCurrentIdx: e });
  },

  handleTabChange(index) {
    this.setTabData(index);
  },

  /**
   * Sets tab data for the selected tab
   * @param {*} index
   */
  setTabData(index) {
    let groupType = PACKAGE_GROUPS[index];
    const packages = this.data.packageMap.get(groupType);
    const currentPackage = packages.find(packageObj => packageObj.package_code == this.data.smartCard.package_code);

    this.setData({
      packages: packages,
      currentPackage: currentPackage != undefined ? currentPackage : null
    });
  },

  groupPackagesByPackageGroup(packages) {
    return packages.reduce((packageMap, currentPackage) => {
      const { package_group } = currentPackage;

      if (!packageMap.has(package_group)) {
        packageMap.set(package_group, [currentPackage]);
      } else {
        packageMap.get(package_group).push(currentPackage);
      }

      return packageMap;
    }, new Map());
  },
  retry() {
    app.closeModal(this);
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  closeModal() {
    app.closeModal(this);
  },
  cancelPayment() {
    my.reLaunch({
      url: INDEX_ROUTE
    });
  }
});

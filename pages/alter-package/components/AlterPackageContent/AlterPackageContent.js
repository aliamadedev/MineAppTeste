import { EMPTY_STRING, HIPHEN_STRING, PACKAGE_GROUPS } from "/constants/app_constants";
import { formatCurrency, logd } from "/utils/utils";

const app = getApp();

Component({
  mixins: [],
  data: {
    selectedPackage: null,
    isLoadingPaymentPrevalidation: false
  },
  props: {
    keyPrefix: PACKAGE_GROUPS[0],
    isLoadingPackages: false,
    currentTab: 0,
    currentPackage: undefined,
    selectedtPackage: undefined,
    packages: undefined,
    isLoadingPaymentPrevalidation: false
  },
  didMount() {
    const computedCurrentPackage = this.props.currentPackage || this.props.packages[0];

    this.setData({
      i18n: app.i18n,
      selectedPackage: computedCurrentPackage,
      currentPackageName: this.props.currentPackage != undefined ? this.props.currentPackage.package_name : app.i18n.common.unavailable,
      isLoadingPaymentPrevalidation: this.props.isLoadingPaymentPrevalidation
    });

    this.setSmartCardPackageOptionsSection();
    this.setDurations(computedCurrentPackage != undefined ? computedCurrentPackage.package_code : null);
  },
  didUpdate() {},
  didUnmount() {},
  methods: {
    onContinue(e) {
      this.setData({ isLoadingPaymentPrevalidation: false });

      const { onTapContinue } = this.props;
      onTapContinue({
        packageCode: this.data.selectedPackage.package_code,
        packagePrice: this.data.selectedDuration.price,
        priceOption: this.data.selectedDuration.value,
        packageName: this.data.selectedPackage.package_name
      });
    },
    // Sheet Events
    saveSheetRef(ref) {
      this.sheetRef = ref;
    },
    saveModalRef(ref) {
      this.modalRef = ref;
    },
    openSheet() {
      this.sheetRef.show();
    },
    closeSheet() {
      // If is visible
      if (this.sheetRef.data.visible) {
        this.sheetRef.hide();
      }
    },
    onRadioChange: function(e) {
      const selectedIndex = this.data.packageDurations.findIndex(item => item.value === e.detail.value);
      const selectedDuration = this.data.packageDurations.find(item => item.value === e.detail.value);
      this.setData({
        selectedDuration: selectedDuration
      });
      const newDurations = this.data.packageDurations.map((item, index) => {
        const newItem = {
          ...item
        };
        newItem.checked = index === selectedIndex ? true : false;
        return newItem;
      });

      this.setData({
        packageDurations: newDurations
      });
    },

    setDurations(selectedPackageCode) {
      try {
        const foundPackage = this.props.packages.find(packagesObj => packagesObj.package_code == selectedPackageCode);
        const availableDurations = foundPackage.price_options.map((option, index) => {
          const mappedDuration = {
            name: `${app.i18n.durationDescription[option.code]} - ${formatCurrency(option.price, 0, app.i18n.common.unavailable)}`,
            value: option.code,
            price: option.price
          };

          if (index === 0) {
            mappedDuration.checked = true;
          }

          return mappedDuration;
        });
        this.setData({
          packageDurations: availableDurations,
          isLoadingSmartCardDurations: false,
          ...(availableDurations.length && {
            selectedDuration: availableDurations[0]
          })
        });
      } catch (err) {
        this.setData({
          isLoadingSmartCardDurations: false,
          showToast: true,
          toastContent: this.data.i18n.common.genericErrorToast,
          toastType: "warning"
        });
        console.error(err);
      }
    },

    setSmartCardPackageOptionsSection() {
      const sections = this.props.packages.map(packagesObj => {
        return {
          title: undefined,
          titleStyle: "display: flex; flex-direction: row; min-height: 20px; margin-bottom: 0px",
          data: [
            {
              leftTitle: packagesObj.package_name,
              rightTitle: EMPTY_STRING,
              rightDescription: EMPTY_STRING,
              rightIcon: packagesObj.package_name === (this.data.selectedPackage != null ? this.data.selectedPackage.package_name : EMPTY_STRING),
              onRowClick: () => {
                this.setData({
                  selectedPackage: packagesObj,
                  isLoadingSmartCardDurations: true
                });
                this.setDurations(packagesObj.package_code);
                this.closeSheet();
                this.setSmartCardPackageOptionsSection();
              },
              rowData: {
                smartCardPackage: packagesObj
              }
            }
          ],
          showSectionButton: false,
          sectionButtonLabel: "",
          sectionButtonAction: () => {
            my.navigateTo({
              url: "/pages/payments-history/payments-history"
            });
          }
        };
      });

      this.setData({
        sections,
        isLoadingSmartCards: false
      });
    }
  }
});

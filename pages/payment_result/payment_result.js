import { ADD_FAVORITE_ROUTE, EMPTY_STRING, PAYMENT_RECEIPT_ROUTE } from "/constants/app_constants";
import { routeBuilder } from "/utils/utils";

const app = getApp();

Page({
  data: {
    showToast: false,
    toastContent: "",
    toastType: 'success',
  },
  onLoad(query) {
    this.setData({
      smartCardName: query.smartCardName || '',
      smartCardNo: query.smartCardNo || '',
      smartCardNoExists: query.smartCardNo ? app.favouriteSmartCards.find(smartCard => smartCard.reference === query.smartCardNo) : false,
      amount: query.amount || "",
      i18n: app.i18n,
      receiptData: app.navigateToPayload
    });
  },
  onShow() {
    my.hideBackHome();
  },

  onHide() {
    this.onToastClose();
  },
  // Methods
  onContinue() {
    let {
      smartCardNo
    } = this.data;

    my.navigateTo({
      url: routeBuilder(ADD_FAVORITE_ROUTE, {smartCardNo : smartCardNo || EMPTY_STRING, fromManageFavorites : false})
    });
  },
  onViewVoucher(e) {
    // TODO: navigate to Receipt page
    my.navigateTo({
      url: '/pages/payment-details/payment-details'
    });
  },
  onFinishFlow() {
    my.reLaunch({
      url: '/pages/index/index'
    }); // back until index
  },

  handleDownloadReceipt() {
    my.navigateTo({
      url: routeBuilder(PAYMENT_RECEIPT_ROUTE, {
        amount: app.navigateToPayload.amount,
        packageName: app.navigateToPayload.subscription,
        receiptNumber: app.navigateToPayload.receiptNumber,
        smartCardNumber: app.navigateToPayload.smartCardNumber,
        duration: app.navigateToPayload.duration,
        paidDate: `${app.navigateToPayload.createDate}, ${app.navigateToPayload.createTime}`,
      })
    });

  },

  textCopied() {
    this.setData({
      showToast: true,
      toastContent: this.data.i18n.paymentResult.copiedSuccess,
      toastType: ""
    });
  },

  textNotCopied() {
    this.setData({
      showToast: true,
      toastContent: this.data.i18n.paymentResult.copiedError,
      toastType: ""
    });
  },
  openToast() {
    this.setData({
      showToast: true
    });
  },
  onToastClose() {
    this.setData({
      showToast: false
    });
  }
});
import { EMPTY_STRING, MANAGE_FAVORITES_ROUTE, SMARTCARD_DETAILS_ROUTE, SMARTCARD_NUMBER_LENGTH_LIMIT } from "/constants/app_constants.js";
import { routeBuilder } from "/utils/utils";
import { smartCardNoValidator } from "/utils/validator";

const app = getApp();

Page({
  data: {
    smartCardName: EMPTY_STRING,
    smartCardNo: EMPTY_STRING,
    isValid: false,
    isLoading: false,
    isNewSmartCard: true,
    isSmartCardNoValid: false,
    isLoading: false,
    inputFieldLengthLimit: SMARTCARD_NUMBER_LENGTH_LIMIT
  },
  onLoad(query) {
    // TODO: Load cached PIN
    const { smartCardName, smartCardNo, amount } = query;

    if (smartCardName && smartCardNo) {
      this.setData({
        smartCardName: smartCardName,
        smartCardNo: smartCardNo,
        smartCardWasFavourite: app.favouriteSmartCards.find(smartCard => smartCard.reference === smartCardNo),
        amount: amount || EMPTY_STRING,
        isNewSmartCard: false
      });
    } else if (smartCardNo) {
      this.setData({
        smartCardNo: smartCardNo,
        smartCardWasFavourite: app.favouriteSmartCards.find(smartCard => smartCard.reference === smartCardNo),
        amount: amount || EMPTY_STRING,
        isNewSmartCard: false
      });
    } else if (app.navigateBackPayload) {
      this.setData({
        smartCardName: app.navigateBackPayload.smartCardName,
        smartCardNo: app.navigateBackPayload.smartCardNo,
        smartCardWasFavourite: app.favouriteSmartCards.find(smartCard => smartCard.reference === app.navigateBackPayload.reference),
        amount: app.navigateBackPayload.amount || EMPTY_STRING,
        isNewSmartCard: false
      });
      app.navigateBackPayload = null;
    }
    this.setData({ i18n: app.i18n });

    // Pre-validate smartCard number
    this.smartCardNoValidator();
  },

  onShow() {
    if (app.navigateBackPayload) {
      this.setData({
        smartCardName: app.navigateBackPayload.name,
        smartCardNo: app.navigateBackPayload.reference,
        smartCardWasFavourite: app.favouriteSmartCards.find(smartCard => smartCard.reference === app.navigateBackPayload.reference),
        isNewSmartCard: false
      });
      app.navigateBackPayload = null;
      // Pre-validate smartCard number
      this.smartCardNoValidator();
    }
  },
  events: {
    onBack() {
      if (app.navigateBackPayload != null) {
        app.navigateBackPayload = null;
      }
    }
  },

  // Methods
  handleInputChange(e) {
    this.setData({ [e.target.dataset.field]: e.detail.value });
    if (e.target.dataset.field == "smartCardNo") {
      this.setData({ smartCardName: EMPTY_STRING });
    }
  
    const smartCard = app.favouriteSmartCards.find(smartCard => smartCard.reference === e.detail.value)
    let cardName = smartCard != undefined ? smartCard.name :  app.i18n.makePayment.smartCardNumber

    this.setData({
      smartCardName: cardName,
      smartCardWasFavourite: smartCard != undefined,
    });
  },
  async onContinue(e) {
    my.navigateTo({ url: routeBuilder(SMARTCARD_DETAILS_ROUTE, { smartCardNo: this.data.smartCardNo || EMPTY_STRING }) });
  },
  onSelectFavourite(e) {
    let { smartCardName, smartCardNo } = this.data;
    my.navigateTo({ url: routeBuilder(MANAGE_FAVORITES_ROUTE, { smartCardNo: smartCardNo || EMPTY_STRING, selectFav: true, smartCardName: smartCardName || EMPTY_STRING }) });
  },
  smartCardNoValidator(e) {
    let { smartCardNo } = this.data;
    let result = smartCardNoValidator(smartCardNo, app.i18n);
    // If did pass the validation and has 11 digits
    if (result === null && smartCardNo.length === 11) {
      this.setData({ isSmartCardNoValid: true });
    } else {
      this.setData({ isSmartCardNoValid: false });
    }

    return result;
  }
});

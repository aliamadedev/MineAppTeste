import DataConverter from "/business/DataConverter";
import {
  ADD_FAVORITE_ROUTE,
  DASHBOARD_NUM_PAYMENTS,
  DEFAULT_LANGUAGE,
  EMPTY_STRING,
  INDEX_ROUTE,
  MANAGE_FAVORITES_ROUTE,
  PAYMENT_DETAILS_ROUTE,
  PAYMENT_HISTORY_ROUTE,
  PAY_NEW_CARD_ROUTE,
  SMARTCARD_DETAILS_ROUTE
} from "/constants/app_constants";
import { NETWORK_UNAVAILABLE, PACKAGE_FETCH_ERROR } from "/constants/network_constants";
import { loadLanguage } from "/i18n/i18n";
import { errorHandler } from "/network/networkManager";
import { formatCurrency, logd, routeBuilder } from "/utils/utils";
const app = getApp();

Page({
  data: {
    showToast: false,
    toastContent: EMPTY_STRING,
    toastType: "success",
    isLoadingSmartCards: true,
    isLoadingPayments: true,
    isLoadingSmartCardPackages: true,
    sections: [],
    isHidden: false,
    carouselCurrentIdx: 0
  },
  // Lifecycle Events
  onLoad(query) {
    // When page load
    this.parsePayments();

    this.setData({ favouriteSmartCards: app.favouriteSmartCards, payments: app.payments, i18n: app.i18n });
  },
  onShow() {
    my.hideBackHome();
    this.setData({
      carouselCurrentIdx: 0,
      favouriteSmartCards: app.favouriteSmartCards,
      payments: app.payments,
      showToast: app.showToast,
      toastContent: app.toastContent,
      toastType: app.toastType
    });
    if (this.data.isHidden) {
      this.setData({ isLoadingSmartCards: false, isHidden: false });
    }
    app.payments && this.parsePayments();
  },
  onReady() {
    // Initialize all needs
    //TODO: to be removed when the fix was done on the apps side
    setTimeout(() => {
      this.initialize();
    }, 500);
  },
  onHide() {
    // When page is hidden
    this.onToastClose();
  },
  events: {
    onBack() {
      this.onToastClose();
    }
  },
  onUnload() {
    // When page is closed
  },
  onTitleClick() {
    // When title is clicked
  },
  onPullDownRefresh() {
    // When page is pulled down
  },
  onReachBottom() {
    // When page is pulled to the bottom
  },
  onShareAppMessage() {
    // Return to custom sharing information
    return {
      title: "StarTimes",
      desc: "StarTimes is a national TV provider",
      path: INDEX_ROUTE
    };
  },

  // Methods
  async initialize() {
    this.setData({ isLoadingSmartCards: true, isLoadingPayments: true, i18n: app.i18n });

    try {
      loadLanguage(DEFAULT_LANGUAGE);
      this.setData({ i18n: app.i18n });

      if (app.globalData.networkAvailable) {
        await this.fetchFavouriteSmartCards();
        await this.fetchPaymentHistory();
        await this.fetchSmartCardPackages();
      } else {
        errorHandler(NETWORK_UNAVAILABLE, this, { fromSelectedFavorite: false, showCancel: false });
      }
    } catch (err) {
      this.setData({ isLoadingSmartCards: false, isLoadingPayments: false });
    }
  },
  async queryLanguageCode() {
    try {
      // Query Customer Info
      loadLanguage(DEFAULT_LANGUAGE);
      this.setData({ isLoading: false, i18n: app.i18n });
    } catch (err) {
      throw err;
    }
  },
  async fetchFavouriteSmartCards() {
    try {
      // Fetch customer favourite smart cards

      let { beneficiaries } = await app.fetchFavouriteSmartCards();
      if (beneficiaries) {
        beneficiaries = beneficiaries.sort((a, b) => a.name.localeCompare(b.name));
      }
      app.favouriteSmartCards = beneficiaries;

      this.setData({ favouriteSmartCards: beneficiaries, isLoadingSmartCards: false });
    } catch (err) {
      this.setData({ isLoadingSmartCards: false, showToast: true, toastContent: this.data.i18n.common.genericErrorToast, toastType: "warning" });
      console.log(err);
    }
  },
  async fetchPaymentHistory() {
    try {
      // Fetch customer Payment History OR Transactions
      const payments = await app.fetchPaymentsHistory(1);
      app.setPayments(payments.data);
      this.setData({ payments: payments.data, isLoadingPayments: false });
      this.parsePayments();
    } catch (err) {
      this.setData({ isLoadingPayments: false, showToast: true, toastContent: this.data.i18n.common.genericErrorToast, toastType: "warning" });
      console.log(err);
    }
  },
  async fetchSmartCardPackages() {
    try {
      // Fetch all packages
      const res = await app.fetchSmartCardPackages();
      app.smartCardPackages = res.packages;
      app.globalData.canPerformPayments = true;
    } catch (err) {
      console.error(err);
      errorHandler(PACKAGE_FETCH_ERROR, this, { fromSelectedFavorite: false });
    }
  },
  // Event handler
  goToPayment(e) {
    if (app.globalData.canPerformPayments) {
      my.navigateTo({ url: PAY_NEW_CARD_ROUTE });
    } else {
      errorHandler(PACKAGE_FETCH_ERROR, this, { fromSelectedFavorite: false });
    }
  },
  onTapPayment(e) {
    app.navigateToPayload = e.target.dataset.value;
    my.navigateTo({ url: routeBuilder(PAYMENT_DETAILS_ROUTE, { canRepay: true }) });
  },
  onStart() {},
  manageFavourites() {
    if (!this.data.isLoadingSmartCards) {
      my.navigateTo({ url: MANAGE_FAVORITES_ROUTE });
    }
  },
  onTapFavourite(e) {
    if (app.globalData.canPerformPayments) {
      const smartCard = e.target.dataset.value;
      const fromSelectedFavorite = true;
      my.navigateTo({
        url: routeBuilder(SMARTCARD_DETAILS_ROUTE, {
          fromSelectedFavorite: fromSelectedFavorite,
          smartCardName: smartCard.name || EMPTY_STRING,
          smartCardNo: smartCard.reference || EMPTY_STRING,
          smartCardPackageName: smartCard.package_name || EMPTY_STRING,
          packagCode: smartCard.package_code || EMPTY_STRING
        })
      });
    } else {
      errorHandler(PACKAGE_FETCH_ERROR, this, { fromSelectedFavorite: false });
    }
  },
  goAddFavourite() {
    if (!this.data.isLoadingSmartCards) {
      my.navigateTo({ url: ADD_FAVORITE_ROUTE });
      this.setData({ isLoadingSmartCards: true, isHidden: true });
    }
  },
  /**
   * manageCarouselIdx: Set the current active smart card position
   * @param { Event } e
   */
  manageCarouselIdx(e) {
    this.setData({ carouselCurrentIdx: e.detail.current });
  },
  openToast() {
    app.showToast = true;
    this.setData({ showToast: true });
  },
  onToastClose() {
    app.showToast = false;
    app.resetToastProps();
    this.setData({ showToast: false });
  },
  parsePayments() {
    this.setData({
      sections: app.payments.length
        ? [
            {
              title: app.i18n.home.paymentLastPayment,
              data: app.payments.slice(0, DASHBOARD_NUM_PAYMENTS).map(payment => {
                let paymentInfo = DataConverter.convertDataToPaymentInfo(payment, app.favouriteSmartCards);

                return {
                  leftTitle: paymentInfo.smartCardName,
                  leftDescription: paymentInfo.smartCardNumber,
                  rightTitle: formatCurrency(paymentInfo.amount, 0, app.i18n.common.unavailable),
                  rightDescription: paymentInfo.createDate ? paymentInfo.createDate : "",
                  onRowClick: paymentInfo => {
                    app.navigateToPayload = { paymentInfo: paymentInfo, data: DataConverter.createReceiptObject(payment) };
                    my.navigateTo({ url: routeBuilder(PAYMENT_DETAILS_ROUTE, { canRepay: true }) });
                  },
                  rowData: paymentInfo
                };
              }),
              showSectionButton: true,
              sectionButtonLabel: app.i18n.home.viewMorePayment,
              sectionButtonAction: () => {
                my.navigateTo({ url: PAYMENT_HISTORY_ROUTE });
              }
            }
          ]
        : []
    });
  },
  /* Error Modal */
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  retry() {
    my.navigateTo({
      url: INDEX_ROUTE
    });
  },
  cancelOperation() {
    my.navigateBack();
  },
  /*End Error Modal */
  onBackButtonTap() {
    my.navigateBack();
  },
  onNoPackagesCancel() {
    app.closeModal(this);
    app.globalData.canPerformPayments = false;
  },
  retry() {
    my.reLaunch({ url: INDEX_ROUTE });
  }
});

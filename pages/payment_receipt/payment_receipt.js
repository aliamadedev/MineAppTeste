import base64 from "base-64";
import { formatCurrency, formatValueToQueryParam, loge } from "/utils/utils";
import { EXTERNAL_API_PDF } from "/constants/app_constants";

const app = getApp();

Page({
  data: {
    amount: "",
    packageName: null,
    receiptNumber: null,
    smartCardNumber: null,
    duration: null,
    paidDate: null,
    downloadInProgress: false,
    showToast: false,
    toastContent: "",
    toastType: "success"
  },
  onLoad(query) {
    const { amount, packageName, receiptNumber, smartCardNumber, duration, paidDate } = query;
    this.setData({
      i18n: app.i18n,
      amount: formatCurrency(amount, 0, app.i18n.common.unavailable),
      packageName: packageName,
      receiptNumber: receiptNumber,
      smartCardNumber: smartCardNumber,
      duration: duration,
      paidDate: paidDate
    });
  },
  onHide() {
    this.onToastClose();
  },
  events: {
    onBack() {
      this.onToastClose();
    }
  },
  onShow() {},
  /**
   * Request's server for the pdf preview and adds the capability for the user
   * to save it or download it
   */
  async onDownloadReceipt() {
    this.setData({ downloadInProgress: true });

    const { amount, packageName, receiptNumber, smartCardNumber, duration, paidDate } = this.data;

    try {
      await app.downloadReceipt(amount, packageName, receiptNumber, smartCardNumber, duration, paidDate, this);
      this.setData({ downloadInProgress: false });
    } catch (error) {
      this.setData({ downloadInProgress: false });
      loge("Error while trying to download pdf: " + error);
      app.setToastProps(this.data.i18n.receipt.unableToDownload, "warning");
      this.setData({ showToast: true, toastContent: app.toastContent, toastType: app.toastType });
    }
  },
  openToast() {
    app.showToast = true;
    this.setData({ showToast: true });
  },
  onToastClose() {
    app.showToast = false;
    app.resetToastProps();
    this.setData({ showToast: false });
  },
  retry() {
    this.setData({ downloadInProgress: false });
    app.closeModal(this);
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  
});

import { INDEX_ROUTE } from "/constants/app_constants";

const app = getApp();

Page({
  data: {
    fromPay: false,
  },
  onLoad(query) {
    this.setData({ newSmartCard: { name: query.smartCardName || '', reference: query.smartCardNo || '' }, fromPay: query.fromPay || false, i18n: app.i18n });
  },
  onShow() {
    this.setData({ favouriteSmartCards: app.favouriteSmartCards });
  },
  confirmReplace(e){
    const i18nAlertsReplace = this.data.i18n.alerts.replaceLimit;
    const oldSmartCard = e.target.dataset.value;
    this.setData({oldSmartCard});
    this.showError(i18nAlertsReplace.title, i18nAlertsReplace.body1, oldSmartCard.name || '', this.data.newSmartCard.name || '', 'replaceFavourite', i18nAlertsReplace.primaryBtn, i18nAlertsReplace.secondaryBtn, 'onCancelModal');
  },
  async replaceFavourite() {
    // Favourite to replace
    const oldSmartCard = this.data.oldSmartCard;

    const newSmartCard = this.data.newSmartCard;

    this.setData({ isLoading: true });

    try {
      await app.updateFavouriteSmartCard(oldSmartCard, this.data.newSmartCard, true, this);

      const oldSmartCardReference = oldSmartCard.reference;

      // Replace the old SmartCard with new SmartCard
      const favouriteSmartCards = [...app.favouriteSmartCards]
        .map((smartCard) => {
          if (smartCard.reference === oldSmartCardReference) {
            smartCard.name = newSmartCard.name;
            smartCard.reference = newSmartCard.reference;
          }
          return smartCard;
        })
        .sort((a, b) => a.name.localeCompare(b.name));

      app.favouriteSmartCards = favouriteSmartCards;

      app.setToastProps(this.data.i18n.replaceFavourite.favouriteReplaceSuccess, 'success');

      if(this.data.fromPay == "true"){
        my.redirectTo({url: INDEX_ROUTE});
      } else {
        my.navigateBack({delta: 2});
      }
      
    } catch (err) {
      app.setToastProps(this.data.i18n.replaceFavourite.favouriteReplaceError, 'warning');
      if(this.data.fromPay == "true"){
        my.redirectTo({url: INDEX_ROUTE});
      } else {
        my.navigateBack({delta: 2});
      }
    } finally {
      //my.hideLoading();
      this.setData({ isLoading: false });
    }
  },
  onCancelModal() {
    this.closeModal();
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  closeModal() {
    // Check modal is visible
    if (this.modalRef.isVisible()) {
      this.modalRef.hide();
      this.setData({
        alertModal: {
          image: '',
          title: '',
          body1: '',
          name1: '',
          name2: '',
          primaryButtonLabel: '',
          primaryButtonOnTap: null,
          secondaryButtonLabel: null,
          secondaryButtonOnTap: null,
        }
      });
    }
  },
  showAlert(title, body1, name1, name2, primaryButtonLabel, primaryButtonOnTap, secondaryButtonLabel, secondaryButtonOnTap, image) {
    this.closeModal();
    this.setData({
      alertModal: {
        image: image,
        title: title,
        body1: body1,
        name1: name1,
        name2: name2,
        primaryButtonLabel: primaryButtonLabel,
        primaryButtonOnTap: primaryButtonOnTap,
        secondaryButtonLabel: secondaryButtonLabel,
        secondaryButtonOnTap: secondaryButtonOnTap,
      }
    });
    this.modalRef.show();
  },
  showError(error, errorMsg, name1, name2, primaryBtnFunction = 'closeModal', primaryButtonLabel = this.data.i18n.common.btnRetry, secondaryButtonLabel = this.data.i18n.common.btnCancel, secondaryBtnFunction = 'closeModal', image = '/assets/illustrations/idea-hi.svg') {
    this.showAlert(error, errorMsg, name1, name2, primaryButtonLabel, primaryBtnFunction, secondaryButtonLabel, secondaryBtnFunction, image);
  },
  retry() {
    this.setData({ isLoading: false });
    app.closeModal(this);
  },
})
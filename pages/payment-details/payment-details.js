import { EMPTY_STRING, INDEX_ROUTE, PAYMENT_RECEIPT_ROUTE, SMARTCARD_DETAILS_ROUTE } from "/constants/app_constants";
import { PACKAGE_FETCH_ERROR } from "/constants/network_constants";
import { errorHandler } from "/network/networkManager";
import { routeBuilder } from "/utils/utils";

const app = getApp();

Page({
  data: {
    i18n: null,
    canRepay: false,
    paymentInfo: null,
    createDateFormated: EMPTY_STRING,
    createTimeFormated: EMPTY_STRING
  },
  onLoad(query) {
    this.setData({
      i18n: app.i18n,
      canRepay: query.canRepay || false,
      paymentInfo: app.navigateToPayload.paymentInfo,
      createDateFormated: app.navigateToPayload.paymentInfo.createDate ? `${app.navigateToPayload.paymentInfo.createDate}` : EMPTY_STRING,
      createTimeFormated: app.navigateToPayload.paymentInfo.createTime ? ` ${app.navigateToPayload.paymentInfo.createTime}` : EMPTY_STRING
    });
  },
  onShow() {},
  onBack() {
    my.navigateBack();
  },

  // Methods
  handleDownloadReceipt() {
    my.navigateTo({
      url: routeBuilder(PAYMENT_RECEIPT_ROUTE, {
        amount: this.data.paymentInfo.amount,
        packageName: this.data.paymentInfo.subscription,
        receiptNumber: this.data.paymentInfo.receiptNumber,
        smartCardNumber: this.data.paymentInfo.smartCardNumber,
        duration: this.data.paymentInfo.duration,
        paidDate: `${this.data.paymentInfo.createDate}, ${this.data.paymentInfo.createTime}`
      })
    });
  },
  onRepay() {
    if (app.globalData.canPerformPayments) {
      const { smartCardName, smartCardNumber, subscription, priceOption, packageCode } = this.data.paymentInfo;

      my.navigateTo({
        url: routeBuilder(SMARTCARD_DETAILS_ROUTE, {
          smartCardName: smartCardName || EMPTY_STRING,
          smartCardNo: smartCardNumber || EMPTY_STRING,
          smartCardPackageName: subscription || EMPTY_STRING,
          priceOption: priceOption || EMPTY_STRING,
          packageCode: packageCode || EMPTY_STRING,
          repay: true
        })
      });
    } else {
      errorHandler(PACKAGE_FETCH_ERROR, this, { fromSelectedFavorite: false });
    }
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  onNoPackagesCancel() {
    app.closeModal(this);
    app.globalData.canPerformPayments = false;
  },
  retry() {
    my.reLaunch({ url: INDEX_ROUTE });
  }
});

import { EMPTY_STRING, FAVOURITE_LIMIT, REPLACE_FAVORITE_ROUTE } from "/constants/app_constants";
import { FAVORITE_NUM_LIMIT } from "/constants/network_constants";
import { errorHandler } from "/network/networkManager";
import { routeBuilder, sortFavourites } from "/utils/utils";
import { smartCardNameValidator, smartCardNoValidator } from "/utils/validator";

const app = getApp();

Page({
  data: {
    defaultTitle: "Add favourite",
    smartCardName: "",
    smartCardNo: "",
    smartCardPackage: "",
    isLoading: false,
    isSmartCardNameValid: false,
    isSmartCardNoValid: false,
    fromManageFavorites: true
  },
  // Lifecycle Events
  onLoad(query) {
    this.setData({ i18n: app.i18n, smartCardNo: query.smartCardNo != undefined ? query.smartCardNo : EMPTY_STRING, fromManageFavorites: query.fromManageFavorites });
  },
  onReady() {},
  onShow() {},
  // Event handler
  handleInputChange(e) {
    this.setData({ [e.target.dataset.field]: e.detail.value });
  },
  smartCardNameValidator(e) {
    let { smartCardName, i18n } = this.data;

    let result = smartCardNameValidator(smartCardName, i18n);

    // If did pass the validation and has at least 3 character
    if (result === null && smartCardName.trim().length >= 3) {
      // Find a favourite with same name based on the user's favourites
      let foundFavourite = app.favouriteSmartCards.find(smartCard => smartCard.name.toLowerCase() === smartCardName.toLowerCase());

      // If found a favourite with same name
      if (foundFavourite) {
        result = i18n.error.smartCardNameAlreadyExist;
        this.setData({ isSmartCardNameValid: false });
      } else {
        this.setData({ isSmartCardNameValid: true });
      }
    } else {
      this.setData({ isSmartCardNameValid: false });
    }

    return result;
  },
  smartCardNoValidator(e) {
    let { smartCardNo, i18n } = this.data;

    let result = smartCardNoValidator(smartCardNo, i18n);

    // If did pass the validation and has 11 digits
    if (result === null && smartCardNo.length === 11) {
      // Find a favourite with same reference based on the user's favourites
      let foundFavourite = app.favouriteSmartCards.find(smartCard => smartCard.reference === smartCardNo);

      // If found a favourite with same reference
      if (foundFavourite) {
        result = i18n.error.smartCardNoAlreadyExist;
        this.setData({ isSmartCardNoValid: false });
      } else {
        this.setData({ isSmartCardNoValid: true });
      }
    } else {
      this.setData({ isSmartCardNoValid: false });
    }

    return result;
  },
  async onAdd(e) {
    //If has the limit number of favourite meters
    if (app.favouriteSmartCards.length >= FAVOURITE_LIMIT) {
      errorHandler(FAVORITE_NUM_LIMIT, this, null)
    } else {
      // Close Alert if opened
      this.closeModal();
      this.setData({ isLoading: true });

      const newSmartCard = {
        name: this.data.smartCardName.trim(),
        reference: this.data.smartCardNo,
        package: this.data.smartCardPackage // TODO - fetch the package information (possibly in the response body)
      };
      try {
        await app.addFavouriteSmartCard(newSmartCard, this);

        const checkIfFavouriteExists = app.favouriteSmartCards.find(f => f.name === newSmartCard.name && f.reference === newSmartCard.reference);
        // Prevent possible duplicates
        if (!checkIfFavouriteExists) {
          const favouriteSmartCards = sortFavourites([...app.favouriteSmartCards, ...[newSmartCard]]);
          app.favouriteSmartCards = favouriteSmartCards;
        }
        app.setToastProps(this.data.i18n.addFavourite.favouriteAddSuccess, "success");

        if (this.data.fromManageFavorites) {
          my.navigateBack();
        } else {
          app.finishFlow();
        }
      } catch (err) {
        app.setToastProps(this.data.i18n.addFavourite.favouriteAddError, "warning");
        my.navigateBack();
      } finally {
        //my.hideLoading();
        this.setData({ isLoading: false });
      }
    }
  },
  onConfirmReplace() {
    const { smartCardName, smartCardNo } = this.data;
    this.closeModal();
    // Go to replace favourite page
    my.navigateTo({
      url: routeBuilder(REPLACE_FAVORITE_ROUTE, {
        smartCardNo: this.data.smartCardNo || EMPTY_STRING,
        smartCardName: smartCardName || EMPTY_STRING,
        fromPay: !this.data.fromManageFavorites,
      })
    });
  },
  onCancelModal() {
    this.closeModal();
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  closeModal() {
    // Check modal is visible
    if (this.modalRef.isVisible()) {
      this.modalRef.hide();
      this.setData({
        alertModal: {
          image: "",
          title: "",
          body1: "",
          primaryButtonLabel: "",
          primaryButtonOnTap: null,
          secondaryButtonLabel: null,
          secondaryButtonOnTap: null
        }
      });
    }
  },

  retry() {
    this.setData({ isLoading: false });
    app.closeModal(this);
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
});

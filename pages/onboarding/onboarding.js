import {
  LS_ONBOARDING_KEY,
  ONBOARDING_INIT_AMOUNT,
  CURRENCY
} from "/constants/app_constants";

const app = getApp();

Page({
  data: {
    i18n: app.i18n,
    onBoardAmount: `${ONBOARDING_INIT_AMOUNT} ${CURRENCY}`,
    currency: CURRENCY,
    carouselCurrentIdx: 0,
    buttonLabel: app.i18n.onboarding.step1.button
  },
  onLoad() {

  },
  onReady() {
    if (my.canIUse('hideBackHome')) {
      my.hideBackHome();
    }
  },

  /**
   * manageCarouselIdx: Set the current active smart card position
   * @param { Event } e
   */
  manageCarouselIdx(e) {
    const i = e.detail.current;
    this.setData({
      carouselCurrentIdx: i
    });
    if (i == 1) {
      this.setData({
        buttonLabel: app.i18n.onboarding.step2.button
      });
    } else {
      this.setData({
        buttonLabel: app.i18n.onboarding.step1.button
      });
    }
  },
  // Methods
  handleFinishOnboarding() {    
    let { carouselCurrentIdx } = this.data;
    /* */
    if (carouselCurrentIdx == 1) {
      // Mark the onboarding as being complete (in the local storage)
      my.setStorage({
        key: LS_ONBOARDING_KEY,
        data: "true",
        fail: function () {
          console.error("Failed to store onboarding completion flag.");
        }
      });

      // Send user to home page
      my.redirectTo({
        url: '/pages/index/index'
      });
    } else {
      this.setData({
        carouselCurrentIdx: 1,
        buttonLabel: app.i18n.onboarding.step2.button
      });
    }
  }
});
import { smartCardNameValidator, smartCardNoValidator } from "/utils/validator";

const app = getApp();

Page({
  data: {
    smartCardNo: "",
    smartCardName: "",
    oldSmartCard: null,
    isLoading: false,
    isSmartCardNameValid: false,
    isSmartCardNoValid: false
  },
  // Lifecycle Events
  onLoad(query) {
    const reference = query.ref;
    const smartCard = app.favouriteSmartCards.find(smartCard => smartCard.reference === reference);
    // Get favourite list without current favourite
    this.favouriteSmartCards = [...app.favouriteSmartCards].filter(smartCard => smartCard.reference !== reference);
    this.setData({
      smartCardName: (smartCard.name || "").trim(),
      smartCardNo: smartCard.reference || "",
      oldSmartCard: { name: (smartCard.name || "").trim(), reference: smartCard.reference || "" },
      i18n: app.i18n
    });
    this.smartCardNameValidator();
    this.smartCardNoValidator();
  },
  onShow() {},
  onReady() {},

  // Event handler
  handleInputChange(e) {
    this.setData({ [e.target.dataset.field]: e.detail.value });
  },
  smartCardNameValidator(e) {
    let { smartCardName, i18n } = this.data;

    let result = smartCardNameValidator(smartCardName, i18n);

    if (result === null && smartCardName.length >= 3) {
      let foundFavourite = this.favouriteSmartCards.find(smartCard => smartCard.name.toLowerCase() === smartCardName.toLowerCase());

      if (foundFavourite) {
        result = i18n.error.smartCardNameAlreadyExist;
        this.setData({ isSmartCardNameValid: false });
      } else {
        this.setData({ isSmartCardNameValid: true });
      }
    } else {
      this.setData({ isSmartCardNameValid: false });
    }

    return result;
  },
  smartCardNoValidator(e) {
    const { smartCardNo, i18n } = this.data;

    let result = smartCardNoValidator(smartCardNo, i18n);

    if (result === null && smartCardNo.length === 11) {
      let foundFavourite = this.favouriteSmartCards.find(smartCard => smartCard.reference === smartCardNo);
      if (foundFavourite) {
        result = i18n.error.smartCardNoAlreadyExist;
        this.setData({ isSmartCardNoValid: false });
      } else {
        this.setData({ isSmartCardNoValid: true });
      }
    } else {
      this.setData({ isSmartCardNoValid: false });
    }

    return result;
  },
  async onSave(e) {
    const { smartCardName, smartCardNo, oldSmartCard } = this.data;

    if (smartCardName !== oldSmartCard.name || smartCardNo !== oldSmartCard.reference) {
      const newSmartCard = {
        name: smartCardName,
        reference: smartCardNo
      };

      this.setData({ isLoading: true });
      try {
        await app.updateFavouriteSmartCard(this.data.oldSmartCard, newSmartCard, false, this);

        const oldSmartCardReference = this.data.oldSmartCard.reference;

        const favouriteSmartCards = [...app.favouriteSmartCards]
          .map(smartCard => {
            if (smartCard.reference === oldSmartCardReference) {
              smartCard.name = newSmartCard.name;
              smartCard.reference = newSmartCard.reference;
            }
            return smartCard;
          })
          .sort((a, b) => a.name.localeCompare(b.name));

        app.favouriteSmartCards = favouriteSmartCards;

        app.setToastProps(this.data.i18n.editFavourite.favouriteEditSuccess, "success");

        my.navigateBack();
      } catch (err) {
        app.setToastProps(this.data.i18n.editFavourite.favouriteEditError, "warning");
        my.navigateBack();
      } finally {
        this.setData({ isLoading: false });
      }
    } else {
      my.navigateBack();
    }
  },
  retry() {
    this.setData({ isLoading: false });
    app.closeModal(this);
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
});

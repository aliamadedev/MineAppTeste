import { ALTER_PACKAGE_ROUTE, CONFIRM_PAYMENT_ROUTE, CURRENCY, EMPTY_STRING, INDEX_ROUTE, PAY_NEW_CARD_ROUTE, ZERO_INT } from "/constants/app_constants";
import { PACKAGE_LIST_EMPTY } from "/constants/network_constants";
import { errorHandler } from "/network/networkManager";
import { formatCurrency, logd, routeBuilder } from "/utils/utils";

const app = getApp();

Page({
  data: {
    smartCardName: EMPTY_STRING,
    smartCardNo: EMPTY_STRING,
    smartCardPackageName: EMPTY_STRING,
    clientFullName: EMPTY_STRING,
    dueDate: EMPTY_STRING,
    amount: undefined,
    displayAmount: undefined,
    priceOption: undefined,
    packageCode: undefined,
    repay: false,
    isLoadingSmartCard: true,
    showToast: false,
    toastContent: EMPTY_STRING,
    toastType: "success",
    fromSelectedFavorite: false
  },
  onLoad(query) {
    const { smartCardNo, smartCardPackageName, packageCode, fromSelectedFavorite, priceOption, repay } = query;

    this.setData({
      fromSelectedFavorite,
      smartCardNo,
      smartCardPackageName,
      priceOption,
      repay,
      i18n: app.i18n
    });

    if (smartCardNo && smartCardPackageName && packageCode) {
      this.setData({
        smartCardNo,
        smartCardPackageName,
        packageCode
      });
    } else if (smartCardNo) {
      const smartCard = app.favouriteSmartCards.find(favouriteSmartCard => favouriteSmartCard.reference === smartCardNo);

      if (smartCard) {
        this.setData({
          ...this.data,
          smartCardNo,
          smartCardName: smartCard.name,
          smartCardPackageName: smartCard.package_name,
          packageCode: smartCard.package_code
        });
      }
    } else if (app.navigateBackPayload) {
      this.setData({
        ...this.data,
        smartCardName: app.navigateBackPayload.smartCardName,
        smartCardNo: app.navigateBackPayload.smartCardNo
      });
      app.navigateBackPayload = null;
    }
  },
  onReady() {
    this.fetchSmartCardDetails(this.data.smartCardNo);
  },
  async onTapContinue() {
    this.showLoadingOnButton(true);

    try {
      const res = await app.fetchPreValidation(
        this.data.packageCode,
        this.data.selectedDuration.price,
        this.data.selectedDuration.value,
        this.data.packageName,
        this.data.smartCardNo,
        this
      );
      const serviceFee = res.data.charge_amount;
      app.navigateToPayload = { serviceFee };

      my.navigateTo({
        url: routeBuilder(CONFIRM_PAYMENT_ROUTE, {
          smartCardNo: this.data.smartCardNo,
          amount: this.data.selectedDuration.price,
          packageCode: this.data.packageCode,
          priceOption: this.data.selectedDuration.value,
          packageName: this.data.packageName,
          customerName: this.data.customerName
        })
      });

      this.showLoadingOnButton(false);
    } catch (err) {
      this.showLoadingOnButton(false);

      console.error(err);
      errorHandler(err.error, this, {
        fromSelectedFavorite: this.data.fromSelectedFavorite
      });
    }
  },
  onTapAlter() {
    my.navigateTo({
      url: routeBuilder(ALTER_PACKAGE_ROUTE, {
        customerName: this.data.customerName,
        smartCardPackageName: this.data.packageName,
        smartCardNo: this.data.smartCardNo,
        packageCode: this.data.packageCode,
        fromSelectedFavorite: this.data.fromSelectedFavorite
      })
    });
  },
  // Sheet Events
  saveSheetRef(ref) {
    this.sheetRef = ref;
  },
  saveModalRef(ref) {
    this.modalRef = ref;
  },
  openSheet() {
    this.sheetRef.show();
  },
  showLoadingOnButton(loading) {
    this.setData({
      isLoading: loading
    });
  },
  closeSheet() {
    // If is visible
    if (this.sheetRef.data.visible) {
      this.sheetRef.hide();
    }
  },
  onRadioChange: function(e) {
    const selectedIndex = this.data.smartCardPackageDurations.findIndex(item => item.value === e.detail.value);
    const selectedDuration = this.data.smartCardPackageDurations.find(item => item.value === e.detail.value);
    this.setData({
      selectedDuration: selectedDuration
    });
    const newSmartCardDurations = this.data.smartCardPackageDurations.map((item, index) => {
      const newItem = {
        ...item
      };
      newItem.checked = index === selectedIndex ? true : false;
      return newItem;
    });

    this.setData({
      smartCardPackageDurations: newSmartCardDurations
    });
  },
  closeModal() {
    if (this.getPreviousPageRoute().includes("index")) {
      my.reLaunch({
        url: INDEX_ROUTE
      });
    } else {
      my.navigateTo({
        url: PAY_NEW_CARD_ROUTE
      });
    }
  },
  cancelPayment() {
    my.reLaunch({
      url: INDEX_ROUTE
    });
  },
  getPreviousPageRoute() {
    const pages = getCurrentPages();
    if (pages.length > 2) {
      return pages[pages.length - 2].__proto__.route;
    } else {
      // index page
      return pages[0].__proto__.route;
    }
  },
  async fetchSmartCardDetails(smartCardNo) {
    try {
      let favouriteSmartCardDetails = app.favouriteSmartCardsDetailed.find(smartCard => smartCard.smartcard_number === smartCardNo);
      if (!favouriteSmartCardDetails) {
        const res = await app.fetchSmartCardDetails(smartCardNo, this);
        favouriteSmartCardDetails = res.data;
        app.favouriteSmartCardsDetailed.push(favouriteSmartCardDetails);
      }

      this.setData({
        smartCardNo: smartCardNo,
        customerName: favouriteSmartCardDetails.customer_name,
        packageCode: favouriteSmartCardDetails.subscribed_package_code,
        packageName: favouriteSmartCardDetails.subscribed_package_name,
        balance: formatCurrency(favouriteSmartCardDetails.balance, 0, favouriteSmartCardDetails.balance === 0 ? `${ZERO_INT} ${CURRENCY}` : app.i18n.common.unavailable)
      });

      this.getSmartCardDurations();
      this.setData({
        isLoadingSmartCard: false
      });
    } catch (err) {
      this.setData({
        isLoadingSmartCard: false
      });
      console.error(err);
      errorHandler(err.error, this, {
        fromSelectedFavorite: this.data.fromSelectedFavorite
      });
    }
  },
  getSmartCardDurations() {
    try {
      const smartCardPackages = app.smartCardPackages;

      // Dont allow customer to proceed if package API fails
      if (smartCardPackages.length === 0) {
        throw Error("No TV subscription packages found");
      }

      const currentSmartCardPackage = smartCardPackages.find(smartCardPackage => smartCardPackage.package_code == this.data.packageCode) || {};
      const availableDurations = currentSmartCardPackage.price_options.map((option, index) => {
        const mappedDuration = {
          displayDuration: `${app.i18n.durationDescription[option.code]}`,
          displayAmount: ` ${formatCurrency(option.price, 0, app.i18n.common.unavailable)}`,
          value: option.code,
          price: option.price
        };

        if (index === 0 && !this.data.repay) {
          mappedDuration.checked = true;
          this.setData({ selectedDuration: mappedDuration });
        } else {
          if (option.code == this.data.priceOption && this.data.repay) {
            mappedDuration.checked = true;
            this.setData({ selectedDuration: mappedDuration });
          }
        }

        return mappedDuration;
      });
      this.setData({
        smartCardPackageDurations: availableDurations,
        ...availableDurations.length
      });
    } catch (err) {
      console.error(err);
      errorHandler(PACKAGE_LIST_EMPTY, this, {
        fromSelectedFavorite: this.data.fromSelectedFavorite
      });
    }
  },

  retry() {
    my.navigateBack();
  }
});

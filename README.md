# Startimes TVSubscriptions Mini-app 📺

Welcome to the Startimes TVSubscriptions Mini-app repository! Our code is like the script for your favorite TV show 🍿, and this guide is your director's manual for maintaining an organized and top-notch codebase 🎥. Dive in and let's make this MiniApp a blockbuster! 🚀

## Table of Contents

- [Branch Naming Conventions](#branch-naming-conventions)
- [Git Commit Message Standards](#git-commit-message-standards)
- [Pull Request Message Standards](#pull-request-message-standards)
- [Checking Out, Rebasing, and Cherry-Picking](#checking-out-rebasing-and-cherry-picking)
- [Feature Branches](#feature-branches)
- [GitHub Issues for Out-of-Sprint Issues](#github-issues-for-out-of-sprint-issues)


---

## Branch Naming Conventions 🌱

### Bug Fix: 🐛

When it's time to squish those pesky bugs, we've got a branch for that! Our `bugfix/` branches are your trusty bug exterminators. They're here to address known issues and make our codebase as smooth as a TV remote.

Example: `bugfix/AVG-12_fix_user_login_issue`

### Task: 📋

Tasks are the building blocks of our MiniApp storyline. Each `task/` branch represents a specific job. Whether it's adding new features or creating a character profile, these branches keep our work neatly organized.

Example: `task/AVG-15_create_user_profile_page`

### Feature: 🚀

Lights, camera, action! When it's time to introduce exciting new features to our MiniApp, we roll out the `feature/` branches. Think of them as the stage for grand performances, where multiple tasks come together to steal the show.

Example: `feature/AVG-20_add_payment_gateway_integration`

### Release: 🎉

Cue the drumroll! `release/` branches are our backstage areas where we prepare for the big release. These branches set the stage for final testing and ensure that our MiniApp is ready to shine in the spotlight.

Example: `release/AVG-2.0_v1.0.0_beta`

---

## Git Commit Message Standards 📝

Our commit messages are like scene descriptions in a script 🎬. They help everyone understand what's happening behind the scenes. Let's make sure they're as clear and engaging as the plot of your favorite TV series.

Use the following template for commit messages:

<CategoryID>: <Short Description>`

- Detailed Change 1
- Detailed Change 2

Example:

`WAKS-12: Add remove button to manage screen`

- Refactor remove logic
- Optimize delete logic



### Mentioning Other Changes within Commit Messages:

Sometimes, there's a subplot within a commit. Mention any related changes in the same commit as bullet points under the main commit message.

---

## Pull Request Message Standards 📤

Our pull requests are like plot twists that keep our MiniApp engaging. Let's make sure they're thrilling and easy to follow, just like your favorite TV series.

### Related Issue:

Think of this as the reference to the main plot. Link to the related issue(s) to give everyone the context they need.

### Dependencies:

Every good storyline has dependencies, and our pull requests are no different. If your PR relies on another one, mention it. It's like saying, "Watch the previous episodes first."

### Description and Changes:

This is where you spill the beans. Provide a detailed description of what's happening in your PR. Tell us about the exciting changes and improvements you're bringing to the MiniApp.

### Impacted Sections in the Application:

Every episode affects different parts of the story. List the sections of the application that will be impacted by your changes. It's like giving a sneak peek of what to expect.

### Testing Instructions:

A good director gives clear instructions, right? Offer step-by-step instructions for testing your changes. Let's make sure the audience (our users) loves the show!

**Example Pull Request: AVG-12**

Related Issue: [AVG-12](link-to-issue)

Dependencies: This pull request is dependent on [PR-XX](link-to-dependent-pr), which should be merged first for proper integration.

Description and Changes:

This pull request addresses the task AVG-12, which involves adding a new feature - the remove button - to the manage screen. In addition, it includes code improvements for better performance and optimization of the delete logic.

Impacted Sections in the Application:

The changes in this pull request impact the following sections of the application:
- Manage screen
- Delete functionality

Testing Instructions:

To verify the changes, follow these steps:
1. Go to the 'Manage' screen in the application.
2. Observe the presence of the new 'Remove' button.
3. Click on the 'Remove' button and verify the expected behavior.
4. Test the delete functionality to ensure it works correctly.
5. Perform any additional testing relevant to the changes made.

By following these instructions, you can confirm that the new 'Remove' button functions as expected, and the code improvements do not introduce any regressions in the 'Delete' functionality.

---

## Checking Out, Rebasing, and Cherry-Picking 🍒

Welcome to our version of script editing! Here's how we make sure our code is ready for the next blockbuster release.

### Checking Out from Develop:

Just like actors rehearse their lines, always check out a new branch from the `develop` branch. It's the starting point for your coding journey.

### Rebasing onto Develop:

Think of this as our script revisions. After completing your work in a task branch, rebase it onto the `develop` branch. It's like incorporating the latest script changes into your scene.

### Cherry-Picking for Feature Releases:

Imagine you have a special episode to release. In such cases, use `git cherry-pick <commit-hash>` to select specific scenes (commits) and include them in another branch. It's like creating a spin-off episode!

---

## Feature Branches 🌟

Sometimes, a single task isn't enough to tell the whole story. That's where feature branches come in!

### When to Use Feature Branches:

Feature branches are like mini-episodes within a season. Use them when a feature requires multiple tasks or substantial changes. Each feature branch encapsulates all related work, making it a separate storyline.

### Responsibility of the Feature Branch Developer:

You're the showrunner of your feature branch. Keep it constantly up to date (rebased) with the `develop` branch as long as other tasks are in progress. We want your feature to be in sync with the main plot!

---

## GitHub Issues for Out-of-Sprint Issues 🔍

In the world of TV, not everything goes according to the script. Sometimes, unexpected issues arise. That's where GitHub Issues come into play.

While our project management system, such as Jira, may handle planned tasks, GitHub Issues are our safety net for the unexpected plot twists.

### When to Use GitHub Issues:

1. **Out-of-Sprint Issues:** If you encounter a bug or problem that wasn't in the script but needs immediate attention, consider creating a GitHub Issue. It's like calling for a quick script rewrite.

2. **Community Feedback:** GitHub Issues are also your channel for audience feedback. If viewers (users) report issues or have suggestions, use GitHub Issues to keep track.

3. **Long-Term Enhancements:** Have grand ideas for future seasons? Create GitHub Issues to store and discuss these long-term enhancements. It's like planning for future episodes.

### Benefits of Using GitHub Issues:

GitHub Issues are like our script annotations. They're visible to the entire team, easy to track, and help us categorize and prioritize tasks. Plus, they seamlessly integrate with pull requests, ensuring we're always on the same page.

---

This README guide serves as your director's cut for maintaining consistent practices in branch naming, commit messages, pull requests, and feature branches. Adhering to these standards will help us create a blockbuster MiniApp together.

Please refer to this guide when crafting your code, and if you have any questions or need clarification, our team is just a message away. Lights, camera, code! 🎬

---

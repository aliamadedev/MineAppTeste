import { DEFAULT_LANGUAGE, SHORTCODE, LS_ONBOARDING_KEY, SERVICE_PROVIDER, INDEX_ROUTE, ONBOARDING_ROUTE } from "/constants/app_constants";
import { LIST_BEN, CREATE_BEN, UPDATE_BEN, DELETE_BEN, LIST_TRAN, PRE_VAL_TV, LIST_TV_PACKAGES, PRE_VAL, NEW_PAYMENT, DOWNLOAD_RECEIPT } from "/constants/network_constants";
import pt from "/i18n/pt";
import apiCall from "./network/networkManager";
import { logd } from "./utils/utils";

App({
  // Events
  onLaunch() {
    // options.query == {number:1}
  },
  onShow() {
    // If onboarding not already performed by the user, redirect him to onboard page
    const onboardingPerformed = my.getStorageSync({
      key: LS_ONBOARDING_KEY
    }).data;
    if (!onboardingPerformed) {
      console.log("User onboarding not yet performed. Redirecting to page...", this.i18n);
      my.redirectTo({
        url: ONBOARDING_ROUTE
      });
    }

    this.getNetworkStatus()
  },
  // Global data
  globalData: {
    shortcode: SHORTCODE,
    staticAssets: {
      icons: "/assets/images/icons",
      illustrations: "/assets/images/illustrations",
      logos: "/assets/images/logos"
    },
    navigateToPayload: null,
    navigateBackPayload: null,
    networkAvailable : true,
    canPerformPayments: true,
  },
  serviceProvider: SERVICE_PROVIDER,
  payments: [],
  favouriteSmartCards: [],
  smartCardPackages: [],
  favouriteSmartCardsDetailed: [],
  showToast: false,
  toastContent: "",
  toastType: "success",
  languageCode: DEFAULT_LANGUAGE, // Default Language Code
  i18n: pt,
  apiCall: apiCall,

  // Global method
  setPayments(payments) {
    this.payments = payments;
  },
  setFavouriteSmartCards(favouriteSmartCards) {
    this.favouriteSmartCards = favouriteSmartCards;
  },
  setLanguage(languageCode, i18n) {
    this.languageCode = languageCode;
    this.i18n = i18n;
  },
  // Retrieve user's smartcards (beneficiaries)
  fetchFavouriteSmartCards() {
    return this.apiCall(LIST_BEN);
  },
  // Retrieve user smartcard (beneficiary)
  fetchSmartCardDetails(smartCardNo, page) {
    return this.apiCall(PRE_VAL_TV, { smartCardNo }, page);
  },
  // Retrieve all packages
  fetchSmartCardPackages() {
    return this.apiCall(LIST_TV_PACKAGES);
  },
  // Create user's smartcard (beneficiary)
  addFavouriteSmartCard(smartCard, page) {
    return this.apiCall(CREATE_BEN, { smartCard }, page);
  },

  // Update user's smartcard  (beneficiary)
  updateFavouriteSmartCard(oldSmartCard, newSmartCard, isReplace = false, page) {
    return this.apiCall(UPDATE_BEN, { oldSmartCard, newSmartCard, isReplace }, page);
  },
  deleteFavouriteSmartCard(smartCard, page) {
    return this.apiCall(DELETE_BEN, { smartCard }, page);
  },
  // Delete user's smartcard (beneficiary)
  paySubscription(packageName, packageCode, smartCardNo, priceOption, amount, page) {
    return this.apiCall(NEW_PAYMENT, { packageName, packageCode, smartCardNo, priceOption, amount }, page);
  },
  //
  fetchPaymentsHistory(page) {
    return this.apiCall(LIST_TRAN, { page });
  },

  fetchSmartCardDetails(smartCardNo, page) {
    return this.apiCall(PRE_VAL_TV, { smartCardNo }, page);
  },

  //Pre validation request
  fetchPreValidation(packageCode, packagePrice, priceOption, packageName, smartCardNo, page) {
    return this.apiCall(PRE_VAL, { packageCode, packagePrice, priceOption, packageName, smartCardNo }, page);
  },

  //Download receipt request
  downloadReceipt(amount, packageName, receiptNumber, smartCardNumber, duration, paidDate, page) {
    return this.apiCall(DOWNLOAD_RECEIPT, { amount, packageName, receiptNumber, smartCardNumber, duration, paidDate }, page);
  },

  setToastProps(text, type) {
    this.toastContent = text;
    this.toastType = type;
    this.showToast = true;
  },
  resetToastProps() {
    this.toastContent = "";
    this.toastType = "success";
    this.showToast = false;
  },
  finishFlow() {
    my.reLaunch({
      url: INDEX_ROUTE
    }); // back until index
  },
  closeModal(page) {
    // Check modal is visible
    if (page.modalRef.isVisible()) {
      page.modalRef.hide();
      page.setData({
        alertModal: {
          image: "",
          title: "",
          body1: "",
          primaryButtonLabel: "",
          primaryButtonOnTap: null,
          secondaryButtonLabel: null,
          secondaryButtonOnTap: null
        }
      });
    }
  },
  getNetworkStatus() {
    my.getNetworkType({
      success: res => {
        this.globalData.networkAvailable =  res.networkAvailable
      }
    });
  }
});

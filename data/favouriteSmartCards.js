const favouriteSmartCards = [
  {
    name: "1",
    reference: "18239473438",
    package_name: "package_name 1"
  },
  {
    name: "2",
    reference: "12345678902",
    package_name: "package_name 2"
  },
  {
    name: "3",
    reference: "12345678903",
    package_name: "package_name 3"
  },
  {
    name: "4",
    reference: "12345678904",
    package_name: "package_name 4"
  },
  {
    name: "5",
    reference: "12345678905",
    package_name: "package_name 5"
  },
];

export default favouriteSmartCards
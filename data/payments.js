const payments = [
  {
    "receipt_number": "AKRH44e0G3K5",
    "billreferencemnumber": "123456",
    "third_party_id": "234",
    "timeStamp": "23-11-07T12:59:50",
    "shortcode": "900053",
    "user": "258849903958",
    "trx_data": {
        "provider": "",
        "smartcard_number": "123456",
        "account_number": "098734",
        "client_name": "John Wick",
        "package": "Premium Plus",
        "charge_amount": 10.3,
        "amount": "1080",
        "months": "1 Mês"
    }
    },
    {
      "receipt_number": "AKRHG3K5",
      "billreferencenumber": "12345678902",
      "third_party_id": "123123123",
      "shortcode": "900053",
      "user": "258849903958",
      "timestamp": "2023-06-30T00:00:00",
      "trx_data": {
        "provider": "Startimes",
        "smartcard_number": "123343212",
        "account_number": "32323221",
        "client_name": "Sofia Isabel Nunes",
        "package": "Premium plus",
        "charge_amount": 5.1,
        "amount": "1100",
        "months": "1 Mês"
      }
    },
    {
      "receipt_number": "AKRHG3K5",
      "billreferencenumber": "12345678902",
      "third_party_id": "123123123",
      "shortcode": "900053",
      "user": "258849903955",
      "timestamp": "2023-06-30T00:00:00",
      "trx_data": {
        "provider": "Startimes",
        "smartcard_number": "123343212",
        "account_number": "32323221",
        "client_name": "Alberto Junior",
        "package": "Premium plus",
        "charge_amount": 5.1,
        "amount": "700",
        "months": "1 Mês"
      }
    },
    {
      "receipt_number": "AKRHG3K5",
      "billreferencenumber": "12345678902",
      "third_party_id": "123123123",
      "shortcode": "900053",
      "user": "258849903959",
      "timestamp": "2023-06-30T00:00:00",
      "trx_data": {
        "provider": "Startimes",
        "smartcard_number": "123343212",
        "account_number": "32323221",
        "client_name": "Isabel Nunes",
        "package": "Premium plus",
        "charge_amount": 5.1,
        "amount": "1200",
        "months": "1 Mês"
      }
    },
];

export default payments